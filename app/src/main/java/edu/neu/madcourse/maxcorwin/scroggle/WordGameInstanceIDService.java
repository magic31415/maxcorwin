package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.SharedPreferences;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Max on 2/27/17.
 */

public class WordGameInstanceIDService extends FirebaseInstanceIdService {
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";
    private static final String EMAIL_STRING = "Email";

    private DatabaseReference db  = FirebaseDatabase.getInstance().getReference();
    private static final String USERS_TABLE = "users";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        String email = scroggleSettings.getString(EMAIL_STRING, "");

        if(!email.isEmpty()) {
            db.child(USERS_TABLE).child(email).child("token").setValue(refreshedToken);
        }
    }
}
