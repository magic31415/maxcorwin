package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

import edu.neu.madcourse.maxcorwin.R;
import edu.neu.madcourse.maxcorwin.scroggle.models.User;

public class CommunicationActivity extends AppCompatActivity {
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";

    private static final String EMAIL_STRING = "Email";

    private DatabaseReference db;
    private static final String USERS_TABLE = "users";
    private static final String BOARDS_TABLE = "boards";

    private String email, friendEmail, highScoreText, colorCode, oldColorCode, token;
    private long thisHighScore, thatHighScore;
    private TextView thisEmailView, thatEmailView, thisScoreView, thatScoreView;

    private final int GREEN = R.color.green_scroggle;
    private final int RED = R.color.red_scroggle;
    private final int WHITE = R.color.white;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw7_activity_communication);

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        email = scroggleSettings.getString(EMAIL_STRING, "");
        friendEmail = getIntent().getExtras().getString("Friend");
        setViews();

        oldColorCode = colorCode = "000000000";

        db = FirebaseDatabase.getInstance().getReference();
        db.child(USERS_TABLE).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                readFromSnapshotUsers(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                readFromSnapshotUsers(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        db.child(BOARDS_TABLE).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                readFromSnapshotBoards(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                readFromSnapshotBoards(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void readFromSnapshotUsers(DataSnapshot dataSnapshot) {
        User user = dataSnapshot.getValue(User.class);

        if(user != null) {
            if(user.getEmail().equals(toPeriods(email))) {
                thisHighScore = user.getHighScore() * -1;
                highScoreText = "High Score: " + String.valueOf(thisHighScore);
                thisScoreView.setText(highScoreText);
            }
            else if(user.getEmail().equals(friendEmail)) {
                thatHighScore = user.getHighScore() * -1;
                highScoreText = "High Score: " + String.valueOf(thatHighScore);
                thatScoreView.setText(highScoreText);
            }
        }
    }

    private void readFromSnapshotBoards(DataSnapshot dataSnapshot) {
        oldColorCode = colorCode;
        colorCode = dataSnapshot.getValue(String.class);
        colorBasedOnCode();
    }

    private void setViews() {
        thisEmailView = (TextView) findViewById(R.id.this_player_email);
        thatEmailView = (TextView) findViewById(R.id.that_player_email);
        thisScoreView = (TextView) findViewById(R.id.this_player_score);
        thatScoreView = (TextView) findViewById(R.id.that_player_score);

        thisEmailView.setText(toPeriods(email));
        thatEmailView.setText(friendEmail);
    }

    public void increaseScoreAction(View view) {
        if(isNetworkAvailable()) {
            thisHighScore += 10;
            db.child(USERS_TABLE).child(email).child("highScore").setValue(thisHighScore * -1);

            String date = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(new Date());
            db.child(USERS_TABLE).child(email).child("highScoreDate").setValue(date);
        }
        else {
            showInternetUnavailable();
        }
    }

    public void resetScoreAction(View view) {
        if(isNetworkAvailable()) {
            thisHighScore = 0;
            db.child(USERS_TABLE).child(email).child("highScore").setValue(thisHighScore);
            db.child(USERS_TABLE).child(email).child("highScoreDate").setValue("none");
        }
        else {
            showInternetUnavailable();
        }
    }

    public void selectLetterAction(View view) {
        if(isNetworkAvailable()) {
            int index = Integer.valueOf(view.getTag().toString()) - 1;
            String newValue;
            int color;

            if(colorCode.charAt(index) == '1') {
                color = WHITE;
                newValue = "0";
            }
            else {
                color = GREEN;
                newValue = "1";
            }

            view.setBackgroundColor(getResources().getColor(color));
            colorCode = colorCode.substring(0, index) + newValue + colorCode.substring(index + 1);
            db.child(BOARDS_TABLE).child("colorCode").setValue(colorCode);
        }
        else {
            showInternetUnavailable();
        }
    }

    private void colorBasedOnCode() {
        ViewGroup smallBoard = (ViewGroup) findViewById(R.id.test_scroggle_board);
        String diff = getColorCodeDiff();

        for(int i = 0; i < 9; i++) {
            if(diff.charAt(i) == '1') {
                smallBoard.getChildAt(i).setBackgroundColor(getResources().getColor(RED));
            }
            else if(diff.charAt(i) == '0') {
                smallBoard.getChildAt(i).setBackgroundColor(getResources().getColor(WHITE));
            }
        }
    }

    private String getColorCodeDiff() {
        String diff = "";

        for(int i = 0; i < 9; i++) {
            if(oldColorCode.charAt(i) != colorCode.charAt(i)) {
                if(colorCode.charAt(i) == '1') {
                    diff += "1";
                }
                else {
                    diff += "0";
                }
            }
            else {
                diff += '-';
            }
        }
        return diff;
    }

    public void pushNotificationAction(View view) {
        if(isNetworkAvailable()) {
            PushNotification pushNotification = new PushNotification();
            retrieveToken();
            System.out.println("Token " + token);
            pushNotification.sendNotification(token);
        }
        else {
            showInternetUnavailable();
        }
    }

    private void retrieveToken() {
        db.child(USERS_TABLE).child(toCommas(friendEmail)).child("email").setValue(friendEmail + " ");
        db.child(USERS_TABLE).child(toCommas(friendEmail)).child("email").setValue(friendEmail);
        db.child(USERS_TABLE).child(toCommas(friendEmail)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                token = user.getToken();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showToast(String message) {
        Toast.makeText(CommunicationActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void showInternetUnavailable() {
        showToast("Internet Unavailable");
    }

    private String toPeriods(String email) {
        return email.replaceAll(",", ".");
    }

    private String toCommas(String email) {
        return email.replaceAll("\\.", ",");
    }
}
