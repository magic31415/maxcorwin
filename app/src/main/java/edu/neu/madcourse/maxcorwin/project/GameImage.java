package edu.neu.madcourse.maxcorwin.project;

import java.util.ArrayList;

/**
 * Created by izstern on 4/17/17.
 */

public class GameImage {
    private ArrayList<GameShape> fullImage;
    private String timestamp;
    private int shapeType;
    private float xCenter;
    private float yCenter;
    private boolean onBoard;
    private boolean selected;
    private boolean unique;

    public GameImage(String timestamp, Double[] doubleX, Double[] doubleY, Double[] doubleZ) {
        this.timestamp = timestamp;
        this.onBoard = true;
        this.selected = true;
        setFullImage(doubleX, doubleY, doubleZ);
        this.unique = true;
    }

    public GameImage(GameImage image) {
        this.timestamp = image.getTimestamp();
        this.onBoard = true;
        this.selected = true;
        this.fullImage = image.getFullImage();
        this.unique = false;
    }

    private void setFullImage(Double[] doubleX, Double[] doubleY, Double[] doubleZ) {
        fullImage = new ArrayList<>();

        for (int i = 0; i < doubleX.length; i++) {
            fullImage.add(new GameShape(doubleX[i], doubleY[i], doubleZ[i]));
        }
    }

    public static boolean allEqual(ArrayList<GameImage> images) {
        for(int i = 0; i < images.size() - 1; i++) {
            for(int j = 0; j < 3; j++) {
                if(images.get(i).getFullImage().get(j).getColor() != images.get(i + 1).getFullImage().get(j).getColor()) {
                    return false;
                }
            }
        }
        return true;
    }

    public float getXCenter() {
        return xCenter;
    }

    public float getYCenter() {
        return yCenter;
    }

    public void setXCenter(float xCenter) {
        this.xCenter = xCenter;
    }

    public void setYCenter(float yCenter) {
        this.yCenter = yCenter;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ArrayList<GameShape> getFullImage() {
        return fullImage;
    }

    public boolean isOnBoard() {
        return onBoard;
    }

    public void setOnBoard(boolean b) {
        this.onBoard = b;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean b) {
        unique = b;
    }

    public int getShapeType() {
        return shapeType;
    }

    public void setShapeType(int shapeType) {
        this.shapeType = shapeType;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
