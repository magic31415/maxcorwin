package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.maxcorwin.R;

public class GameActivity extends AppCompatActivity {
    private static final String GAME_PREFS = "GamePreferenceFile";
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";

    private static final String EXISTING_GAME = "Existing Game";
    private static final String TWO_PLAYER = "Two Player";

    private static final String TIME_REMAINING = "Time Remaining";
    private static final String WORDS_FOUND = "Words Found";
    private static final String CURRENT_BOGGLE_BOARD_ID = "Current Boggle Board Id";
    private static final String FROZEN_BOARDS_IDS = "Frozen Boards Ids";
    private static final String PREVIOUS_LETTER_LOCATION = "Previous Letter Location";
    private static final String WORD_TEXT = "Word Text";
    private static final String PART_TWO = "Part Two";
    private static final String SCORE_STRING = "Score";
    private static final String BOARD_WORDS = "Board Words";
    private static final String BOARD_COLORS = "Board Colors";
    private static final String PAUSED = "Paused";

    private static final String MUSIC_STRING = "Music";
    private static final String SOUNDS_STRING = "Sounds";
    private static final String NEW_GAME = "New Game";

    private static final String TOO_SHORT_MSG = "Words must be at least three characters long.";
    private static final String NOT_WORD_MSG = "Not a valid word.";
    private static final String CANNOT_REUSE_MSG = "Cannot reuse words from part one.";

    private boolean MUSIC, SOUNDS;
    private MediaPlayer MUSIC_PLAYER, SELECT_LETTER_PLAYER, FIND_WORD_PLAYER, GAME_OVER_PLAYER;

    private boolean existingGame;
    private long timeRemaining;
    private CountDownTimer timer;
    private final int ONE_SECOND = 1000;
    private final int TIME_LIMIT = 90 * ONE_SECOND;
    private final int WARMUP_TIME = 2 * ONE_SECOND;

    private ArrayList<String> dictionary = new ArrayList<>(); // excludes 9 letter words
    private ArrayList<String> nineLetterWords = new ArrayList<>();
    private ArrayList<String> wordsFound = new ArrayList<>();

    private final Random rand = new Random();
    private final int NUMBER_OF_SEED_WORDS = 9;
    private final int FULL_WORD_LENGTH = 9;
    private int score = 0;

    private final List<Integer> ORIGINAL_ORDER = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8);

    private View SCROGGLE_BOARD, BLANK_BOARD;
    private TextView TIMER_LABEL, TIMER_VIEW, WORD, SCORE;
    private Button PLAY_PAUSE_BUTTON, CLEAR_BUTTON, SUBMIT_BUTTON;
    private boolean isPaused = false;
    private int GREEN_COLOR, WHITE_COLOR;

    private final ArrayList<Integer> TILES = new ArrayList<>(Arrays.asList(
            R.id.small1, R.id.small2, R.id.small3, R.id.small4, R.id.small5,
            R.id.small6, R.id.small7, R.id.small8, R.id.small9));

    final ArrayList<Integer> LARGE_BOARDS = new ArrayList<>(Arrays.asList(
            R.id.large1, R.id.large2, R.id.large3, R.id.large4, R.id.large5,
            R.id.large6, R.id.large7, R.id.large8, R.id.large9));

    private final HashMap<Integer, ArrayList<Integer>> NEIGHBORS = initNeighbors();
    private final HashMap<Integer, ArrayList<Character>> SCORE_MAP = initScoreMap();
    private final ArrayList<Integer> POINT_AMOUNTS = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 8, 10));

    private ViewParent currentBoggleBoard;
    private int currentBoggleBoardId;
    private ArrayList<ViewParent> frozenBoards = new ArrayList<>();
    private ArrayList<String> frozenBoardIds = new ArrayList<>();


    private int previousLetterLocation;
    private String wordText;

    private String boardWordsAsString = "";
    private String boardColorsAsString = "";

    private boolean partTwo = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw5_activity_game);
        initializeVariables();

        if(getIntent().getExtras().getBoolean(NEW_GAME, true)) {
            freezeAllBoards();
            clearGamePreferences();
        }
        else {
            readFromSharedPreferences();
            PLAY_PAUSE_BUTTON.setVisibility(View.VISIBLE);
            PLAY_PAUSE_BUTTON.setText(R.string.string_resume);
            hideClearSubmit();
            WORD.setText(R.string.string_paused);
            TIMER_LABEL.setVisibility(View.VISIBLE);
            TIMER_VIEW.setText(formatAsSeconds(timeRemaining));
        }

        // Creates dictionary and seeds board
        new DictionaryCreator(getResources()).execute();
    }

    private void initializeVariables() {
        MUSIC_PLAYER = MediaPlayer.create(this, R.raw.ttt_game_music);
        SELECT_LETTER_PLAYER = MediaPlayer.create(this, R.raw.sergenious_movex);
        FIND_WORD_PLAYER = MediaPlayer.create(this, R.raw.beep);
        GAME_OVER_PLAYER = MediaPlayer.create(this, R.raw.oldedgar_winner);

        SCROGGLE_BOARD = findViewById(R.id.scroggle_board);
        BLANK_BOARD = findViewById(R.id.blank_board);
        TIMER_LABEL = (TextView) findViewById(R.id.timer_label);
        TIMER_VIEW = (TextView) findViewById(R.id.timer);
        PLAY_PAUSE_BUTTON = (Button) findViewById(R.id.button_play_pause);
        CLEAR_BUTTON = (Button) findViewById(R.id.button_clear);
        SUBMIT_BUTTON = (Button) findViewById(R.id.button_submit);
        WORD = (TextView) findViewById(R.id.word);
        SCORE = (TextView) findViewById(R.id.score);
        GREEN_COLOR = getResources().getColor(R.color.green_scroggle);
        WHITE_COLOR = getResources().getColor(R.color.white);
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("On Start");
        readFromSharedPreferences();
        existingGame = true;
    }

    private void readFromSharedPreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        timeRemaining = gameState.getLong(TIME_REMAINING, TIME_LIMIT + WARMUP_TIME);
        wordsFound = new ArrayList<>(gameState.getStringSet(WORDS_FOUND, new HashSet<String>()));
        currentBoggleBoardId = gameState.getInt(CURRENT_BOGGLE_BOARD_ID, -1);
        currentBoggleBoard = (ViewParent) findViewById(currentBoggleBoardId);
        frozenBoardIds = new ArrayList<>(gameState.getStringSet(FROZEN_BOARDS_IDS, new HashSet<String>()));
        for(String id : frozenBoardIds) {
            frozenBoards.add((ViewParent) findViewById(Integer.valueOf(id)));
        }
        previousLetterLocation = gameState.getInt(PREVIOUS_LETTER_LOCATION, -1);
        wordText = gameState.getString(WORD_TEXT, "");
        partTwo = gameState.getBoolean(PART_TWO, false);
        SCORE.setText(gameState.getString(SCORE_STRING, "0"));
        score = Integer.valueOf(SCORE.getText().toString());
        boardWordsAsString = gameState.getString(BOARD_WORDS, "");
        boardColorsAsString = gameState.getString(BOARD_COLORS, "");
        isPaused = gameState.getBoolean(PAUSED, false);

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        MUSIC = scroggleSettings.getBoolean(MUSIC_STRING, true);
        SOUNDS = scroggleSettings.getBoolean(SOUNDS_STRING, true);
        existingGame = scroggleSettings.getBoolean(EXISTING_GAME, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("On Resume");

        // create a new timer the first time
        if(timeRemaining > TIME_LIMIT) {
            createTimer(timeRemaining);
        }
        playMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("On Pause");

        if(!isPaused) {
            pauseGame();
        }
        stopMusic();
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("On Stop");
        writeToSharedPreferences();
    }

    private void pauseGame() {
        isPaused = true;
        timer.cancel();
        PLAY_PAUSE_BUTTON.setText(R.string.string_resume);
        hideBoard();
        hideClearSubmit();

        wordText = WORD.getText().toString();
        WORD.setText(R.string.string_paused);
    }

    private void resumeGame() {
        isPaused = false;
        PLAY_PAUSE_BUTTON.setText(R.string.string_pause);
        showClearSubmit();

        WORD.setText(wordText);
        createTimer(timeRemaining);
    }

    public void playPauseAction(View v) {
        if(isPaused) {
            resumeGame();
            showBoard();
        }
        else {
            pauseGame();
        }
    }

    private void writeToSharedPreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = gameState.edit();
        editor.putLong(TIME_REMAINING, timeRemaining);
        editor.putStringSet(WORDS_FOUND, new HashSet<>(wordsFound));
        editor.putInt(CURRENT_BOGGLE_BOARD_ID, currentBoggleBoardId);
        editor.putStringSet(FROZEN_BOARDS_IDS, new HashSet<>(frozenBoardIds));
        editor.putInt(PREVIOUS_LETTER_LOCATION, previousLetterLocation);
        editor.putString(WORD_TEXT, wordText);
        editor.putBoolean(PART_TWO, partTwo);
        editor.putString(SCORE_STRING, String.valueOf(score));
        editor.putString(BOARD_WORDS, boardWordsAsString);
        packUpBoardColors();
        editor.putString(BOARD_COLORS, boardColorsAsString);
        editor.putBoolean(PAUSED, isPaused);
        editor.apply();

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        editor = scroggleSettings.edit();
        editor.putBoolean(EXISTING_GAME, existingGame);
        editor.putBoolean(TWO_PLAYER, false);
        editor.apply();
    }

    private void packUpBoardColors() {
        boardColorsAsString = "";

        for(int board : LARGE_BOARDS) {
            ViewGroup smallBoard = (ViewGroup) findViewById(board);
            View tile;

            for(int i = 0; i < FULL_WORD_LENGTH; i++) {
                tile = smallBoard.getChildAt(i);
                if(((ColorDrawable) tile.getBackground()).getColor() == GREEN_COLOR) {
                    boardColorsAsString += "1";
                }
                else {
                    if(frozenBoards.contains(smallBoard) || ((Button) tile).getText().toString().isEmpty()) {
                        boardColorsAsString += "2";
                    }
                    else {
                        boardColorsAsString += "0";
                    }
                }
            }
        }
    }

    private void clearGamePreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = gameState.edit();
        editor.clear();
        editor.apply();
    }

    private void playMusic() {
        if(MUSIC) {
            MUSIC_PLAYER.setVolume(0.5f, 0.5f);
            MUSIC_PLAYER.setLooping(true);
            MUSIC_PLAYER.start();
        }
    }

    private void playSound(MediaPlayer mediaPlayer) {
        if(SOUNDS) {
            mediaPlayer.setVolume(1.0f, 1.0f);
            mediaPlayer.start();
        }
    }

    private void stopMusic() {
        if(MUSIC) {
            MUSIC_PLAYER.stop();
            MUSIC_PLAYER.release();
        }
    }

    public void restartAction(View v) {
        timer.cancel();
        clearGamePreferences();
        readFromSharedPreferences();
        //partTwo = false;
        TIMER_LABEL.setVisibility(View.GONE);
        PLAY_PAUSE_BUTTON.setText(R.string.string_pause);
        hideBoard();
        clearAllBoards();
        freezeAllBoards();

        WORD.setAllCaps(false);
        WORD.setText(R.string.part_1_instructions);
        SCORE.setText(R.string.string_initial_score);

        CLEAR_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAction(v);
            }
        });
        SUBMIT_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAction(v);
            }
        });

        seedBoard();
        createTimer(timeRemaining);
    }

    private void startPartTwo() {
        partTwo = true;
        hideBoard();
        WORD.setAllCaps(false);
        WORD.setText(R.string.part_2_instructions);

        emptyFrozenBoards();
        clearAllBoards();
        CLEAR_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllBoards();
                WORD.setText("");
            }
        });

        Button submitButton = (Button) findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitPartTwo();
            }
        });

        createTimer(TIME_LIMIT + WARMUP_TIME);
    }

    private void clearAllBoards() {
        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            for(int board : LARGE_BOARDS) {
                currentBoggleBoard = (ViewParent) findViewById(board);
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                clearCurrentBoard();
            }
        }
    }

    public void selectLetterAction(View v) {
        if(BLANK_BOARD.getVisibility() == View.VISIBLE) {
            return;
        }

        final Button tile = (Button) v;
        final ViewParent thisBoggleBoard = tile.getParent();

        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_1_instructions).toString())
                || word.equalsIgnoreCase(getText(R.string.part_2_instructions).toString()) ) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        if(partTwo) {
            selectLetterPartTwo(v);
        }
        else if(!frozenBoards.contains(thisBoggleBoard)) {
            if(currentBoggleBoard == null) {
                currentBoggleBoard = thisBoggleBoard;
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
            else if(currentBoggleBoard.equals(thisBoggleBoard)) {
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
            else {
                clearAction(v);
                currentBoggleBoard = thisBoggleBoard;
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
        }
    }

    private void selectLetterPartTwo(View view) {
        final Button tile = (Button) view;
        int tileColor = ((ColorDrawable) tile.getBackground()).getColor();

        if(!(tile.getText().equals("")
                || tileColor == GREEN_COLOR
                || frozenBoards.size() == NUMBER_OF_SEED_WORDS)) {
            addLetter(tile);
        }
    }

    private boolean isValidNextLetter(Button tile) {
        int nextLetterLocation = Integer.valueOf(tile.getTag().toString());
        int tileColor = ((ColorDrawable) tile.getBackground()).getColor();

        if((previousLetterLocation == -1
                || NEIGHBORS.get(previousLetterLocation).contains(nextLetterLocation))
                && (tileColor != GREEN_COLOR)) {
            previousLetterLocation = nextLetterLocation;
            return true;
        }
        return false;
    }

    private void addLetter(Button letter) {
        letter.setBackgroundColor(GREEN_COLOR);
        playSound(SELECT_LETTER_PLAYER);
        String newWord = formatTextViewString(WORD) + letter.getText().toString().toUpperCase();
        WORD.setText(newWord);
    }

    private void clearCurrentBoard() {
        ViewGroup smallBoard = ((ViewGroup) currentBoggleBoard);
        View tile;

        for(int i = 0; i < FULL_WORD_LENGTH; i++) {
            tile = smallBoard.getChildAt(i);
            tile.setBackgroundColor(WHITE_COLOR);
        }
    }

    private void freezeBoard() {
        ViewGroup smallBoard = ((ViewGroup) currentBoggleBoard);
        Button tile;
        int tileColor;

        for(int i = 0; i < FULL_WORD_LENGTH; i++) {
            tile = (Button) smallBoard.getChildAt(i);
            tileColor = ((ColorDrawable) tile.getBackground()).getColor();

            if(tileColor != GREEN_COLOR) {
                tile.setText("");
            }
        }

        frozenBoards.add(currentBoggleBoard);
        frozenBoardIds.add(Integer.toString(((View) currentBoggleBoard).getId()));
        currentBoggleBoard = null;
        currentBoggleBoardId = -1;
        previousLetterLocation = -1;
    }

    private void freezeAllBoards() {
        for(int boardId : LARGE_BOARDS) {
            currentBoggleBoard =  (ViewParent) findViewById(boardId);
            currentBoggleBoardId = ((View) currentBoggleBoard).getId();

            if(!frozenBoards.contains(currentBoggleBoard)) {
                freezeBoard();
            }
        }
    }

    public void submitAction(View v) {
        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_1_instructions).toString())) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        String foundWord = formatTextViewString(WORD).toLowerCase();

        if(foundWord.isEmpty()) {
            // Do Nothing
        }
        else if(foundWord.length() < 3) {
            showToast(TOO_SHORT_MSG);
        }
        else if(isValidWord(foundWord)) {
            wordsFound.add(foundWord);
            playSound(FIND_WORD_PLAYER);
            freezeBoard();
            score += calculateWordScore();
            SCORE.setText(String.valueOf(score));
            WORD.setText("");
            if(frozenBoards.size() == NUMBER_OF_SEED_WORDS) {
                endGameEarly();
            }
        }
        else {
            showToast(NOT_WORD_MSG);
        }
    }

    private void submitPartTwo() {
        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_2_instructions).toString()) ) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        String foundWord = formatTextViewString(WORD).toLowerCase();

        if(foundWord.isEmpty()) {
            // Do Nothing
        }
        else if(foundWord.length() < 3) {
            showToast(TOO_SHORT_MSG);
        }
        else if(wordsFound.contains(foundWord)) {
            showToast(CANNOT_REUSE_MSG);
        }
        else if(isValidWord(foundWord)) {
            freezeAllBoards();
            score += calculateWordScore();
            SCORE.setText(String.valueOf(score));
            WORD.setText("");
            endGameEarly();
        }
        else {
            showToast(NOT_WORD_MSG);
        }
    }

    private void endGameEarly() {
        TIMER_LABEL.setVisibility(View.GONE);
        timer.cancel();

        PLAY_PAUSE_BUTTON.setVisibility(View.INVISIBLE);
        TIMER_VIEW.setText("");

        if(partTwo) {
            gameOver();
        }
        else {
            startPartTwo();
        }
    }

    public void clearAction(View v) {
        clearSmallBoard();
    }

    private void clearSmallBoard() {
        previousLetterLocation = -1;
        WORD.setText("");
        if(currentBoggleBoard != null) {
            clearCurrentBoard();
        }
    }

    private boolean isValidWord(String foundWord) {
        if(dictionary.contains(foundWord) || nineLetterWords.contains(foundWord)) {
            wordsFound.add(foundWord);
            return true;
        }
        return false;
    }

    private int calculateWordScore() {
        int wordScore = 0;
        String word = formatTextViewString(WORD);

        for(int i = 0; i < word.length(); i++) {
            for(int amount : POINT_AMOUNTS) {
                if(SCORE_MAP.get(amount).contains(word.charAt(i))) {
                    wordScore += amount;
                    break;
                }
            }
        }
        return wordScore;
    }

    private HashMap<Integer, ArrayList<Character>> initScoreMap() {
        HashMap<Integer, ArrayList<Character>> scoreMap = new HashMap<>();
        scoreMap.put(1, new ArrayList<>(Arrays.asList('E', 'A', 'I', 'O', 'N', 'R', 'T', 'L', 'S', 'U')));
        scoreMap.put(2, new ArrayList<>(Arrays.asList('D', 'G')));
        scoreMap.put(3, new ArrayList<>(Arrays.asList('B', 'C', 'M', 'P')));
        scoreMap.put(4, new ArrayList<>(Arrays.asList('F', 'H','V', 'W', 'Y')));
        scoreMap.put(5, new ArrayList<>(Collections.singletonList('K')));
        scoreMap.put(8, new ArrayList<>(Arrays.asList('J', 'X')));
        scoreMap.put(10, new ArrayList<>(Arrays.asList('Q', 'Z')));

        return scoreMap;
    }

    private HashMap<Integer, ArrayList<Integer>> initNeighbors() {
        HashMap<Integer, ArrayList<Integer>> neighbors = new HashMap<>();
        neighbors.put(1, new ArrayList<>(Arrays.asList(2, 4, 5)));
        neighbors.put(2, new ArrayList<>(Arrays.asList(1, 3, 4, 5, 6)));
        neighbors.put(3, new ArrayList<>(Arrays.asList(2, 5, 6)));
        neighbors.put(4, new ArrayList<>(Arrays.asList(1, 2, 5, 7, 8)));
        neighbors.put(5, new ArrayList<>(Arrays.asList(1, 2, 3, 4, 6, 7, 8, 9)));
        neighbors.put(6, new ArrayList<>(Arrays.asList(2, 3, 5, 8, 9)));
        neighbors.put(7, new ArrayList<>(Arrays.asList(4, 5, 8)));
        neighbors.put(8, new ArrayList<>(Arrays.asList(4, 5, 6, 7, 9)));
        neighbors.put(9, new ArrayList<>(Arrays.asList(5, 6, 8)));

        return neighbors;
    }

    private String formatTextViewString(TextView view) {
        return view.getText().toString().toUpperCase();
    }

    private String formatAsSeconds(long millis) {
        return Integer.toString((int) Math.ceil(millis / (double) ONE_SECOND));
    }

    private void showToast(String message) {
        Toast.makeText(GameActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void showBoard() {
        BLANK_BOARD.setVisibility(View.GONE);
        SCROGGLE_BOARD.setVisibility(View.VISIBLE);
    }

    private void hideBoard() {
        SCROGGLE_BOARD.setVisibility(View.GONE);
        BLANK_BOARD.setVisibility(View.VISIBLE);
    }

    private void showClearSubmit() {
        CLEAR_BUTTON.setVisibility(View.VISIBLE);
        SUBMIT_BUTTON.setVisibility(View.VISIBLE);
    }

    private void hideClearSubmit() {
        CLEAR_BUTTON.setVisibility(View.INVISIBLE);
        SUBMIT_BUTTON.setVisibility(View.INVISIBLE);
    }

    private void emptyFrozenBoards() {
        frozenBoards.clear();
        frozenBoardIds.clear();
    }

    public void quitAction(View v) {
        writeToSharedPreferences();
        finish();
    }

    private class DictionaryCreator extends AsyncTask<Void, Void, Void> {
        private Resources resources;
        private final TextView LOADING_SCREEN = (TextView) findViewById(R.id.scroggle_loading_screen);

        DictionaryCreator(Resources resources) {
            this.resources = resources;
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("Start loading dictionary");
            readInDictionaryWords(resources);
            System.out.println("End loading dictionary");
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LOADING_SCREEN.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LOADING_SCREEN.setVisibility(View.INVISIBLE);
            seedBoard();
        }

        private void readInDictionaryWords(Resources resources) {
            String word;

            try {
                InputStream inputStream = resources.openRawResource(R.raw.wordlist);
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);

                while ((word = bufferedReader.readLine()) != null) {
                    word = word.trim();
                    if (word.length() == FULL_WORD_LENGTH) {
                        nineLetterWords.add(word);
                    }
                    else {
                        dictionary.add(word);
                    }
                }
                bufferedReader.close();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void seedBoard() {

        // create new words
        if(boardWordsAsString.isEmpty()) {
            addWordsToBoard();
        }

        // redraw board
        else {
            ArrayList<String> seedWords = new ArrayList<>();

            for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
                int startIndex = i * FULL_WORD_LENGTH;
                int endIndex = startIndex + FULL_WORD_LENGTH;
                seedWords.add(boardWordsAsString.substring(startIndex, endIndex));
            }

            for(int i = 0; i < seedWords.size(); i++) {
                addToBoard(seedWords.get(i), LARGE_BOARDS.get(i));
            }
        }

        System.out.println(boardColorsAsString);
        if(!boardColorsAsString.isEmpty()) {
            unpackBoardColors();
        }
    }

    private void addWordsToBoard() {
        ArrayList<String> seedWords = pickSeedWords();
        System.out.println("Words: " + seedWords);
        for(int i = 0; i < seedWords.size(); i++) {
            addToBoard(shuffleWord(seedWords.get(i)), LARGE_BOARDS.get(i));
        }
    }

    private void addToBoard(String word, int largeBoardId) {
        boardWordsAsString += word;
        for(int i = 0; i < word.length(); i++) {
            Button tile = getTile(TILES.get(i), largeBoardId);
            String letter = word.substring(i, i + 1);
            tile.setText(letter);
        }
    }

    private Button getTile(int tile, int largeBoardId) {
        return (Button) findViewById(largeBoardId).findViewById(tile);
    }

    private ArrayList<String> pickSeedWords() {
        ArrayList<String> seedWords = new ArrayList<>();
        String word;

        while(seedWords.size() < NUMBER_OF_SEED_WORDS) {
            word = nineLetterWords.get(rand.nextInt(nineLetterWords.size()));
            if(!seedWords.contains(word)) {
                seedWords.add(word);
            }
        }
        return seedWords;
    }

    private String shuffleWord(String original) {
        ArrayList<Integer> shuffledOrder = createLetterOrder();
        String shuffled = "";

        for(int i = 0; i < FULL_WORD_LENGTH; i++) {
            shuffled += original.charAt(shuffledOrder.get(i));
        }
        return shuffled;
    }

    private ArrayList<Integer> createLetterOrder() {
        ArrayList<Integer> order = new ArrayList<>(ORIGINAL_ORDER);
        boolean found = false;
        while(!found) {
            Collections.shuffle(order);
            if(isValidOrder(order)) {
                found = true;
            }
        }
        return order;
    }

    private boolean isValidOrder(ArrayList<Integer> shuffled) {
        for(int i = 1; i < FULL_WORD_LENGTH; i++) {
            if(!areNeighbors(shuffled, ORIGINAL_ORDER.get(i - 1), ORIGINAL_ORDER.get(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean areNeighbors(ArrayList<Integer> shuffled, int first, int second) {
        int firstIndex = shuffled.indexOf(first) + 1;
        int secondIndex = shuffled.indexOf(second) + 1;

        return NEIGHBORS.get(firstIndex).contains(secondIndex);
    }

    private void unpackBoardColors() {
        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            ViewGroup smallBoard = (ViewGroup) findViewById(LARGE_BOARDS.get(i));
            View tile;
            int index;

            for(int j = 0; j < FULL_WORD_LENGTH; j++) {
                tile = smallBoard.getChildAt(j);
                index = i * FULL_WORD_LENGTH + j;
                if(boardColorsAsString.charAt(index) == '1') {
                    tile.setBackgroundColor(GREEN_COLOR);
                }
                else if(boardColorsAsString.charAt(index) == '2') {
                    ((Button) tile).setText("");
                }
            }
        }
    }

    private void createTimer(long timerLength) {
        timer = new CountDownTimer(timerLength, 1000) {
            @Override
            public void onTick(long millisRemaining) {
                onCounterTick(millisRemaining);
            }

            @Override
            public void onFinish() {
                clearSmallBoard();
                freezeAllBoards();

                TIMER_LABEL.setVisibility(View.GONE);
                PLAY_PAUSE_BUTTON.setVisibility(View.INVISIBLE);
                TIMER_VIEW.setText(R.string.string_times_up);

                if(partTwo || score == 0) {
                    gameOver();
                }
                else {
                    startPartTwo();
                }

                writeToSharedPreferences();
            }
        };
        timer.start();
    }

    private void onCounterTick(long millisRemaining) {
        timeRemaining = millisRemaining;

        if(timeRemaining > TIME_LIMIT + ONE_SECOND) {
            TIMER_LABEL.setText(R.string.string_time_label);
            TIMER_VIEW.setText(R.string.string_ready);
        }
        else if(timeRemaining > TIME_LIMIT) {
            TIMER_VIEW.setText(R.string.string_set);
        }
        else if(timeRemaining > TIME_LIMIT - ONE_SECOND) {
            emptyFrozenBoards();
            showBoard();
            TIMER_VIEW.setText(R.string.string_go);
            PLAY_PAUSE_BUTTON.setVisibility(View.VISIBLE);
        }
        else if(timeRemaining > TIME_LIMIT - ONE_SECOND * 2) {
            TIMER_LABEL.setVisibility(View.VISIBLE);
            TIMER_VIEW.setText(formatAsSeconds(timeRemaining));
        }
        else if(timeRemaining < ONE_SECOND * 9) {
            TIMER_LABEL.setText(R.string.string_hurry_up);
            TIMER_VIEW.setText(formatAsSeconds(timeRemaining));
        }
        else {
            TIMER_VIEW.setText(formatAsSeconds(timeRemaining));
        }
    }

    private void gameOver() {
        WORD.setText(R.string.string_game_over);
        playSound(GAME_OVER_PLAYER);
        existingGame = false;
        hideClearSubmit();
    }
}
