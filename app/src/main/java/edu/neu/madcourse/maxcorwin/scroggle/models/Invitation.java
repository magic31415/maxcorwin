package edu.neu.madcourse.maxcorwin.scroggle.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Max on 3/21/17.
 */

@IgnoreExtraProperties
public class Invitation {
    private String gameId;

    public Invitation() {
    }

    public String getGameId() {
        return gameId;
    }
}
