package edu.neu.madcourse.maxcorwin.project;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.BufferOverflowException;
import java.util.Random;
import java.util.zip.GZIPInputStream;

import edu.neu.madcourse.maxcorwin.R;

public class TrickiestPartActivity extends AppCompatActivity {
    //private String INPUT_GZIP_FILE = PATH + "/" + "zipped_noon_data.gz";
    final double MAXIMUM_ACCELERATION = 2.5;
    final int MAXIMUM_COLOR_VALUE = 255;
    final Random RAND = new Random();
    Paint paint = new Paint();

    int red, green, blue, timestamp, index, WIDTH, HEIGHT, radius;
    float verticalCenter, horizontalCenter;
    double x, y, z;

    final int ARRAY_SIZE = 288000;
    Double[] doubleX = new Double[ARRAY_SIZE];
    Double[] doubleY = new Double[ARRAY_SIZE];
    Double[] doubleZ = new Double[ARRAY_SIZE];

    private static final String DATA_TAG = "Data";

    final int NUMBER_OF_IMAGES = 12;
    final int SHAPES_PER_IMAGE = 100;
    final int TIME_BETWEEN_SHAPES = 10;
    final int TIMESTAMP_RANGE = SHAPES_PER_IMAGE * TIME_BETWEEN_SHAPES;
    final int MAX_RADIUS = 130;
    final int STEP_SIZE = MAX_RADIUS/SHAPES_PER_IMAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // unzipFile();

        InputStream fis;
        // GZIPInputStream gis;

        try {
            fis = getResources().openRawResource(R.raw.noon_data);
            // gis = new GZIPInputStream(new BufferedInputStream(fis));

            InputStreamReader reader = new InputStreamReader(fis);
            BufferedReader bufferReader = new BufferedReader(reader);

            parser(bufferReader);

        } catch (BufferOverflowException e) {
            e.printStackTrace();
        }

        setContentView(new CircleView(this));
    }

    private void unzipFile() {

        InputStream fis;
        GZIPInputStream gis;

        try {
            fis = getResources().openRawResource(R.raw.zipped_noon_data);
            //fis = new FileInputStream(INPUT_GZIP_FILE);
            gis = new GZIPInputStream(new BufferedInputStream(fis));

            //Log.d(TAG, "gunzipFile: Gunzipping file");

            InputStreamReader reader = new InputStreamReader(gis);
            BufferedReader bufferReader = new BufferedReader(reader);

            parser(bufferReader);

        } catch (IOException | BufferOverflowException e) {
            e.printStackTrace();
        }
    }

    private void parser(BufferedReader bufferReader) {
        String line;
        int counter = -1;

        try {
            String[] parts;
            while ((line = bufferReader.readLine()) != null) {
                parts = line.split(",");
                if (counter > -1) {
                    doubleX[counter] = Double.parseDouble(parts[1]);
                    doubleY[counter] = Double.parseDouble(parts[2]);
                    doubleZ[counter] = Double.parseDouble(parts[3]);
                }
                counter++;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class CircleView extends View {
        public CircleView(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            WIDTH = getWidth();
            HEIGHT = getHeight();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            canvas.drawPaint(paint);

            for(int i = 0; i < NUMBER_OF_IMAGES; i++) {
                for(int j = 0; j < SHAPES_PER_IMAGE; j++) {

                    if(j % 2 == 0) {
                        timestamp = RAND.nextInt(ARRAY_SIZE - TIMESTAMP_RANGE);
                        index = timestamp + (j * TIME_BETWEEN_SHAPES);
                        x = doubleX[index];
                        y = doubleY[index];
                        z = doubleZ[index];
                        Log.d(DATA_TAG, String.valueOf(x + " " + y + " " + z));
                    }

                    verticalCenter = getVerticalCenter(j);
                    horizontalCenter = getHorizontalCenter(j);
                    radius = MAX_RADIUS - (STEP_SIZE * j);
                    paint.setColor(calculateColor(x, y, z));
                    canvas.drawCircle(verticalCenter, horizontalCenter, radius, paint);
                }
            }
        }

        private int calculateColor(double x, double y, double z) {
            red = calculateColorComponent(x);
            green = calculateColorComponent(y);
            blue = calculateColorComponent(z);
            return Color.rgb(red, green, blue);
        }

        private int calculateColorComponent(double accel) {
            return (int) Math.round((Math.abs(accel) / MAXIMUM_ACCELERATION) * MAXIMUM_COLOR_VALUE);
        }

        private float getVerticalCenter(int imageNumber) {
            return (WIDTH/4) * (imageNumber % 3 + 1);
        }

        private float getHorizontalCenter(int imageNumber) {
            return (HEIGHT/5) * (imageNumber % 4 + 1);
        }
    }
}
