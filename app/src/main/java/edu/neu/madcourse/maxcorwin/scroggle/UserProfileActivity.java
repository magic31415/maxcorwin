package edu.neu.madcourse.maxcorwin.scroggle;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import edu.neu.madcourse.maxcorwin.R;
import edu.neu.madcourse.maxcorwin.scroggle.models.Invitation;
import edu.neu.madcourse.maxcorwin.scroggle.models.User;

public class UserProfileActivity extends AppCompatActivity {
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";
    private static final String EMAIL_STRING = "Email";
    private static final String TWO_PLAYER = "Two Player";
    private static final String TOKEN = "Token";
    private static final String GAME_ID_LABEL = "Game ID";

    private boolean twoPlayer;

    private DatabaseReference db;
    private static final String USERS_TABLE = "users";
    private static final String GAMES_TABLE = "games";
    private static final String INVITATIONS_TABLE = "invitation";

    private TextView emailView, highScoreView, dateView, noFriendsView;
    private Button addFriendButton, seeHighScoresButton, logOutButton;
    private EditText addFriendText;
    private LinearLayout addFriendLayout, friendsListLayout, waitingScreen;
    private ScrollView friendsScrollView;

    private LinearLayout playPopupLayout;
    private TextView popupEmail;

    private LinearLayout highScoresLayout;
    private TextView highScoresTextView;

    private String email, friendEmail, date, gameId;
    private ArrayList<String> friends;
    private long highScore;

    private final ArrayList<String> emailAddresses
            = new ArrayList<>(Arrays.asList("maxcorwin@yahoo.com", "otherperson@gmail.com"));

    boolean live;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw7_activity_user_profile);
        db = FirebaseDatabase.getInstance().getReference();
        setViewValues();

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        email = scroggleSettings.getString(EMAIL_STRING, "");
        emailView.setText(toPeriods(email));

        Bundle extras = getIntent().getExtras();
        twoPlayer = extras.getBoolean(TWO_PLAYER);
        if (getIntent().hasExtra(TOKEN)) {
            String token = extras.getString(TOKEN);
            db.child(USERS_TABLE).child(email).child("token").setValue(token);
        }

        db.child(USERS_TABLE).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                handleDataSnapshot(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                handleDataSnapshot(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

        private void handleDataSnapshot(DataSnapshot dataSnapshot) {
        User user = dataSnapshot.getValue(User.class);

        if(user.getEmail().equals(toPeriods(email))) {
            highScore = user.getHighScore() * -1;
            date = user.getHighScoreDate();
            friends = User.makeFriendsList(user.getFriends());

            String highScoreText = "High Score: " + String.valueOf(highScore);
            highScoreView.setText(highScoreText);
            connectedToInternet();

            if(!date.equals("none")) {
                String dateText = "Achieved On: " + date;
                dateView.setText(dateText);
            }

            if(!friends.isEmpty()) {
                showFriends();
                createFriendButtons(); // TODO will this work for more friends
            }
        }
    }

    ValueEventListener friendEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);

            if(user != null && user.getEmail() != null && user.getEmail().equals(toPeriods(friendEmail))) {

                if(friendEmail.equalsIgnoreCase(email)) {
                    showToast("CANNOT ADD SELF");
                }
                else if(friends.contains(friendEmail)) {
                    showToast("ALREADY FRIENDS"); // TODO too often
                }
                else if(!emailAddresses.contains(toPeriods(friendEmail))) {
                    showToast("USER NOT FOUND");
                }
                else {
                    addFriendToList(friendEmail);

                    addFriendText.setText("");
                    addFriendLayout.setVisibility(View.GONE);
                    addFriendButton.setVisibility(View.VISIBLE);
                    showToast("FRIEND ADDED");
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public void addAction(View view) {
        if(isNetworkAvailable()) {
            friendEmail = toCommas(addFriendText.getText().toString().toLowerCase());
            db.child(USERS_TABLE).child(friendEmail).addListenerForSingleValueEvent(friendEventListener);
        }
        else {
            showInternetUnavailable();
        }
    }

    private void addFriendToList(String friendEmail) {
        db.child(USERS_TABLE).child(friendEmail).removeEventListener(friendEventListener);
        friends.add(friendEmail);

        String friendsString = User.makeFriendsString(friends);
        db.child(USERS_TABLE).child(email).child("friends").setValue(friendsString);
    }

    private void createFriendButtons() {
        if(twoPlayer) {
            for(String friendEmail : friends) {
                addFriendButton(friendEmail);
            }
        }
        else {
            for(String friendEmail : friends) {
                addFriendButton(friendEmail);
            }
        }
    }

    private void addFriendButton(final String friendEmail) {
        Button button = new Button(this);
        String buttonText = "Play against " + toPeriods(friendEmail);
        button.setText(buttonText);
        button.setAllCaps(false);

        if(twoPlayer) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPlayPopupAction(view);
                }
            });
        }
        else {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startCommunicationAction(view);
                }
            });
        }
        friendsListLayout.addView(button);
    }

    public void play2PlayerAction(View view) {
        live = ((Button) view).getText().equals("Live");

        db.child(INVITATIONS_TABLE).child(email).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Invitation invitation = dataSnapshot.getValue(Invitation.class);

                if(invitation.getGameId() == null || invitation.getGameId().isEmpty() || invitation.getGameId().equals("0")) {
                    gameId = createNewGame();
                    checkIfInvitationAccepted();
                }
                else {
                    gameId = invitation.getGameId();
                    db.child(INVITATIONS_TABLE).child(email).child("gameId").setValue("0");

                    start2PlayerActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void checkIfInvitationAccepted() {

        db.child(INVITATIONS_TABLE).child(toCommas(friendEmail)).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Invitation invitation = dataSnapshot.getValue(Invitation.class);

                if(invitation != null && invitation.getGameId().equals("0")) {
                    start2PlayerActivity();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void start2PlayerActivity() {
        Class mode;
        waitingScreen.setVisibility(View.GONE);
        playPopupLayout.setVisibility(View.GONE);


        if(live) {
            mode = TwoPlayerLiveGameActivity.class;
        }
        else {
            mode = TwoPlayerUntimedGameActivity.class;
        }

        Intent intent = new Intent(this, mode);
        intent.putExtra(GAME_ID_LABEL, gameId);
        startActivity(intent);
    }

    private String createNewGame() {
        String gameId = String.valueOf(new Random().nextInt());
        // TODO check for internet
        getGame(gameId).child("player1Email").setValue(toPeriods(email));
        getGame(gameId).child("player2Email").setValue(friendEmail);
        getGame(gameId).child("player1Score").setValue(0);
        getGame(gameId).child("player2Score").setValue(0);
        getGame(gameId).child("player1FrozenBoards").setValue(" ");
        getGame(gameId).child("player2FrozenBoards").setValue(" ");
        getGame(gameId).child("player1Done").setValue(false);
        getGame(gameId).child("player2Done").setValue(false);

        final String ALL_WHITE = "000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        getGame(gameId).child("boardColors").setValue(ALL_WHITE);
        getGame(gameId).child("boardWords").setValue("");
        getGame(gameId).child("wordsFound").setValue(" ");
        getGame(gameId).child("partTwo").setValue(false);
        getGame(gameId).child("player1Turn").setValue(true);

        db.child(INVITATIONS_TABLE).child(toCommas(friendEmail)).child("gameId").setValue(gameId);
        waitingScreen.setVisibility(View.VISIBLE);

        return gameId;
    }

    private DatabaseReference getGame(String gameId) {
        return db.child(GAMES_TABLE).child(gameId);
    }

    public void showPlayPopupAction(View view) {
        String popupEmailText = ((Button) view).getText().toString();
        friendEmail = popupEmailText.substring(13);
        popupEmail.setText(popupEmailText);
        playPopupLayout.setVisibility(View.VISIBLE);
    }

    public void hidePlayPopupAction(View view) {
        playPopupLayout.setVisibility(View.GONE);
    }

    public void startCommunicationAction(View view) {
        if(isNetworkAvailable()) {

            Button friendButton = (Button) view;
            friendEmail = friendButton.getText().toString().substring(13);

            Intent intent = new Intent(this, CommunicationActivity.class);
            intent.putExtra("Friend", friendEmail);
            startActivity(intent);
        }
        else {
            showInternetUnavailable();
        }
    }

    private void showFriends() {
        noFriendsView.setVisibility(View.GONE);
        friendsScrollView.setVisibility(View.VISIBLE);
    }

    public void addFriendAction(View view) {
        addFriendButton.setVisibility(View.GONE);
        addFriendLayout.setVisibility(View.VISIBLE);
    }

    public void showHighScoresAction(View view) {
        db.child(USERS_TABLE).orderByChild("highScore").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String highScoreData = "";
                for(DataSnapshot child : dataSnapshot.getChildren()) {
                    User user = child.getValue(User.class);
                    highScoreData += user.getEmail() + " " + user.getHighScore() * -1 + "\n";
                }
                highScoresTextView.setText(highScoreData);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        highScoresLayout.setVisibility(View.VISIBLE);
    }

    public void hideHighScoresAction(View view) {
        highScoresLayout.setVisibility(View.GONE);
    }

    public void logOutAction(View view) {
        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = scroggleSettings.edit();
        editor.putString(EMAIL_STRING, "");
        editor.apply();
        finish();
    }

    private void connectedToInternet() {
        if(logOutButton.getVisibility() == View.INVISIBLE) {
            noFriendsView.setVisibility(View.VISIBLE);
            addFriendButton.setVisibility(View.VISIBLE);
            seeHighScoresButton.setVisibility(View.VISIBLE);
            logOutButton.setVisibility(View.VISIBLE);
        }
    }

    private void setViewValues() {
        emailView = (TextView) findViewById(R.id.email);
        highScoreView = (TextView) findViewById(R.id.high_score);
        dateView = (TextView) findViewById(R.id.date);
        addFriendButton = (Button) findViewById(R.id.add_friend_button);
        seeHighScoresButton = (Button) findViewById(R.id.see_high_scores_button);
        logOutButton = (Button) findViewById(R.id.log_out_button);
        addFriendText = (EditText) findViewById(R.id.friend_email_textbox);
        addFriendText = (EditText) findViewById(R.id.friend_email_textbox);
        addFriendLayout = (LinearLayout) findViewById(R.id.add_friend_layout);
        friendsListLayout = (LinearLayout) findViewById(R.id.friends_layout);
        noFriendsView = (TextView) findViewById(R.id.no_friends);
        friendsScrollView = (ScrollView) findViewById(R.id.friends_scroll_view);

        playPopupLayout = (LinearLayout) findViewById(R.id.play_popup_layout);
        popupEmail = (TextView) findViewById(R.id.popup_email);

        highScoresLayout = (LinearLayout) findViewById(R.id.high_scores_layout);
        highScoresTextView = (TextView) findViewById(R.id.high_scores_text_view);

        waitingScreen = (LinearLayout) findViewById(R.id.waiting_screen);
    }

    public void clearTextboxAction(View view) {
        clearTextbox();
    }

    private void clearTextbox() {
        addFriendText.setText("");
    }

    public void cancelInvitationAction(View view) {
        db.child(INVITATIONS_TABLE).child(toCommas(friendEmail)).child("gameId").setValue("");
        waitingScreen.setVisibility(View.GONE);
        }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showToast(String message) {
        Toast.makeText(UserProfileActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void showInternetUnavailable() {
        showToast("Internet Unavailable");
    }

    private String toPeriods(String email) {
        return email.replaceAll(",", ".");
    }

    private String toCommas(String email) {
        return email.replaceAll("\\.", ",");
    }
}
