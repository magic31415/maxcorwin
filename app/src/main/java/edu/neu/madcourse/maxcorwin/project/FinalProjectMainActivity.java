package edu.neu.madcourse.maxcorwin.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import edu.neu.madcourse.maxcorwin.R;

public class FinalProjectMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw12_activity_final_project_main);
    }

    public void playAction(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.project.FinalProjectGameActivity.class);
        startActivity(intent);
    }

    public void showAckAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.pair_ack_layout);
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void hideAckAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.pair_ack_layout);
        linearLayout.setVisibility(View.GONE);
    }

}
