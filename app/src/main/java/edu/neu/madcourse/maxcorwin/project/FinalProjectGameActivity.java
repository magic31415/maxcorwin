package edu.neu.madcourse.maxcorwin.project;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import edu.neu.madcourse.maxcorwin.R;

public class FinalProjectGameActivity extends AppCompatActivity {
    DatabaseReference db = FirebaseDatabase.getInstance().getReference();

    private static final String PROJECT_PREFS = "Project Preferences";
    private static final String CURRENT_LEVEL = "Current Level Number";
    private static final String SOUND = "Sound";
    private static final String NAME = "Name";

    private final int ARRAY_SIZE = 288000;
    private String[] timestamps = new String[ARRAY_SIZE];
    private Double[] doubleX = new Double[ARRAY_SIZE];
    private Double[] doubleY = new Double[ARRAY_SIZE];
    private Double[] doubleZ = new Double[ARRAY_SIZE];

    private int currentLevel;
    private boolean sound;
    private String name;

    private TextView levelText, namesText, levelsReachedText, messageText;
    private GameBoardView gameBoardView;
    private LinearLayout pairButtons, dotLayout, newLeaderBoardEntryView;
    private ImageButton turnSoundOffButton, turnSoundOnButton;
    private ImageView dot1Empty, dot1Filled, dot2Empty, dot2Filled, dot3Empty, dot3Filled, dot4Empty, dot4Filled;
    private EditText textBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw12_activity_final_project_game);

        initializeViews();

        InputStream fis = getResources().openRawResource(R.raw.noon_data);
        new DataReader(fis).execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        readFromSharedPreferences();
    }

    @Override
    protected void onStop() {
        super.onStop();
        writeToSharedPreferences();
    }

    private void readFromSharedPreferences() {
        SharedPreferences projectState = this.getApplicationContext().getSharedPreferences(PROJECT_PREFS, MODE_PRIVATE);
        currentLevel = projectState.getInt(CURRENT_LEVEL, 1);
        sound = projectState.getBoolean(SOUND, true);
        if(!sound) {
            turnSoundOffButton.setVisibility(View.GONE);
            turnSoundOnButton.setVisibility(View.VISIBLE);
        }
        name = projectState.getString(NAME, "");

    }

    private void writeToSharedPreferences() {
        System.out.println("Write To Shared Preferences");
        SharedPreferences projectState = this.getApplicationContext().getSharedPreferences(PROJECT_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = projectState.edit();
        editor.putInt(CURRENT_LEVEL, currentLevel);
        editor.putBoolean(SOUND, sound);
        editor.putString(NAME, name);
        editor.apply();
    }

    private void initializeViews() {
        levelText = (TextView) findViewById(R.id.current_level);
        namesText = (TextView) findViewById(R.id.names);
        levelsReachedText = (TextView) findViewById(R.id.levels_reached);
        messageText = (TextView) findViewById(R.id.leaderboard_message);

        gameBoardView = (GameBoardView) findViewById(R.id.game_board);
        pairButtons = (LinearLayout) findViewById(R.id.pair_buttons);
        dotLayout = (LinearLayout) findViewById(R.id.dot_layout);
        newLeaderBoardEntryView = (LinearLayout) findViewById(R.id.new_leaderboard_entry);
        textBox = (EditText) findViewById(R.id.name_textbox);

        turnSoundOffButton = (ImageButton) findViewById(R.id.turn_sound_off_button);
        turnSoundOnButton = (ImageButton) findViewById(R.id.turn_sound_on_button);

        dot1Empty = (ImageView) findViewById(R.id.dot_1_empty);
        dot1Filled = (ImageView) findViewById(R.id.dot_1_filled);
        dot2Empty = (ImageView) findViewById(R.id.dot_2_empty);
        dot2Filled = (ImageView) findViewById(R.id.dot_2_filled);
        dot3Empty = (ImageView) findViewById(R.id.dot_3_empty);
        dot3Filled = (ImageView) findViewById(R.id.dot_3_filled);
        dot4Empty = (ImageView) findViewById(R.id.dot_4_empty);
        dot4Filled = (ImageView) findViewById(R.id.dot_4_filled);
    }

    // Reset Button
    public void resetAction(View view) {
        gameBoardView.resetLevel();
    }


    // Show Leaderboard Button
    public void showLeaderBoardAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.leaderboard_layout);
        linearLayout.setVisibility(View.VISIBLE);
        generateLeaderBoard();
    }

    private void generateLeaderBoard() {
        if(isNetworkAvailable()) {
            db.child("pairScores").orderByValue().limitToLast(10).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String namesString = "";
                    String levelsString = "";
                    for(DataSnapshot child : dataSnapshot.getChildren()) {
                        namesString = child.getKey() + "\n" + namesString;
                        levelsString = child.getValue() + "\n" + levelsString;
                    }
                    messageText.setText("");
                    namesText.setText(namesString);
                    levelsReachedText.setText(levelsString);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    messageText.setText(R.string.string_pair_scores_error);
                    namesText.setText("");
                    levelsReachedText.setText("");
                }
            });
        }
        else {
            messageText.setText(R.string.string_no_network_connection);
            namesText.setText("");
            levelsReachedText.setText("");
        }
    }

    private void getThresholdAndAddUser() {
        if(isNetworkAvailable()) {
            db.child("pairScores").orderByValue().limitToLast(10).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for(DataSnapshot child : dataSnapshot.getChildren()) {
                        int threshold = Integer.valueOf(child.getValue().toString());
                        if(currentLevel > threshold) {
                            addUserToLeaderBoard();
                        }
                        break;
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    private void addUserToLeaderBoard() {
        if(isNetworkAvailable()) {
            if (name.equals("")) {
                newLeaderBoardEntryView.setVisibility(View.VISIBLE);
            }
            else {
                updateUser();
            }
        }
    }

    public void addUser(View view) {
        String enteredText = textBox.getText().toString();
        if(!enteredText.equals("")) {
            name = textBox.getText().toString();
            updateUser();
            newLeaderBoardEntryView.setVisibility(View.GONE);
            showLeaderBoardAction(view);
        }
    }

    private void updateUser() {
        db.child("pairScores").child(name).setValue(currentLevel);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // Hide Leaderboard Button
    public void hideLeaderBoardAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.leaderboard_layout);
        linearLayout.setVisibility(View.GONE);
    }

    // Show Data Info Button
    public void showDataInfoAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.data_scientist_blurb_layout);
        linearLayout.setVisibility(View.VISIBLE);
    }

    // Hide Data Info Button
    public void hideDataInfoAction(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.data_scientist_blurb_layout);
        linearLayout.setVisibility(View.GONE);
    }

    // Sound Off Button
    public void turnSoundOffAction(View view) {
        sound = false;
        turnSoundOffButton.setVisibility(View.GONE);
        turnSoundOnButton.setVisibility(View.VISIBLE);
    }

    // Sound On Button
    public void turnSoundOnAction(View view) {
        sound = true;
        turnSoundOnButton.setVisibility(View.GONE);
        turnSoundOffButton.setVisibility(View.VISIBLE);
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void incrementCurrentLevel() {
        currentLevel += 1;
        getThresholdAndAddUser();
        levelText.setText(String.valueOf(currentLevel));
    }

    public boolean hasSound() {
        return sound;
    }

    private class DataReader extends AsyncTask<Void, Integer, Void> {
        private InputStream fis;

        private final TextView loadingScreen = (TextView) findViewById(R.id.pair_loading);

        DataReader(InputStream fis) {
            this.fis = fis;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            gameBoardView.setVisibility(View.GONE);
            pairButtons.setVisibility(View.INVISIBLE);
            loadingScreen.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loadingScreen.setVisibility(View.GONE);
            gameBoardView.setVisibility(View.VISIBLE);
            dotLayout.setVisibility(View.VISIBLE);
            pairButtons.setVisibility(View.VISIBLE);
            levelText.setText(String.valueOf(currentLevel));
            gameBoardView.setLevel(currentLevel);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                parser();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void parser() throws IOException {
            // gis = new GZIPInputStream(new BufferedInputStream(fis));
            InputStreamReader reader = new InputStreamReader(fis);
            BufferedReader bufferReader = new BufferedReader(reader);

            String line;
            int counter = -1;

            try {
                String[] parts;
                while ((line = bufferReader.readLine()) != null) {
                    parts = line.split(",");
                    if (counter > -1) {
                        timestamps[counter] = parts[0];
                        doubleX[counter] = Double.parseDouble(parts[1]);
                        doubleY[counter] = Double.parseDouble(parts[2]);
                        doubleZ[counter] = Double.parseDouble(parts[3]);
                    }
                    counter++;
                }
                int i;
                for(i = 0; i < doubleX.length; i ++) {
                    if(doubleX[i] == null) {
                        break;
                    }
                }
            }
            finally {
                bufferReader.close();
            }
        }
    }

    public String[] getTimestamps() {
        return timestamps;
    }

    public Double[] getDoubleX() {
        return doubleX;
    }

    public Double[] getDoubleY() {
        return doubleY;
    }

    public Double[] getDoubleZ() {
        return doubleZ;
    }

    public void fillDot(int dotNum) {
        if (dotNum == 1) {
            dot1Empty.setVisibility(View.GONE);
            dot1Filled.setVisibility(View.VISIBLE);
        }
        else if (dotNum == 2) {
            dot2Empty.setVisibility(View.GONE);
            dot2Filled.setVisibility(View.VISIBLE);
        }
        else if (dotNum == 3) {
            dot3Empty.setVisibility(View.GONE);
            dot3Filled.setVisibility(View.VISIBLE);
        }
        else if (dotNum == 4) {
            dot4Empty.setVisibility(View.GONE);
            dot4Filled.setVisibility(View.VISIBLE);
        }
    }

    // Initialize Empty Dots
    public void initDots(int pairSize) {
        dot1Empty.setVisibility(View.GONE);
        dot2Empty.setVisibility(View.GONE);
        dot3Empty.setVisibility(View.GONE);
        dot4Empty.setVisibility(View.GONE);

        dot1Filled.setVisibility(View.GONE);
        dot2Filled.setVisibility(View.GONE);
        dot3Filled.setVisibility(View.GONE);
        dot4Filled.setVisibility(View.GONE);

        if(pairSize >= 2) {
            dot1Empty.setVisibility(View.VISIBLE);
            dot2Empty.setVisibility(View.VISIBLE);
        }

        if (pairSize >= 3) {
            dot3Empty.setVisibility(View.VISIBLE);
        }
        if (pairSize == 4) {
            dot4Empty.setVisibility(View.VISIBLE);
        }
    }
}
