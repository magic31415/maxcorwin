package edu.neu.madcourse.maxcorwin.scroggle.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Max on 3/16/17.
 */

@IgnoreExtraProperties
public class Game {
    String player1Email;
    String player2Email;
    int player1Score;
    int player2Score;

    String player1FrozenBoards;
    String player2FrozenBoards;
    boolean player1Done;
    boolean player2Done;

    String boardWords;
    String boardColors; /* | 0 = white | 1 = p1 | 2 = p2 | 3 = brown | 4 = blank | */
    boolean partTwo;
    String wordsFound;
    boolean player1Turn;

    public Game() {
    }

    public String getPlayer1Email() {
        return player1Email;
    }

    public String getPlayer2Email() {
        return player2Email;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    public String getPlayer1FrozenBoards() {
        return player1FrozenBoards;
    }

    public String getPlayer2FrozenBoards() {
        return player2FrozenBoards;
    }

    public boolean getPlayer1Done() {
        return player1Done;
    }

    public boolean getPlayer2Done() {
        return player2Done;
    }

    public String getBoardWords() {
        return boardWords;
    }

    public String getBoardColors() {
        return boardColors;
    }

    public boolean isPartTwo() {
        return partTwo;
    }

    public String getWordsFound() {
        return wordsFound;
    }

    public boolean isPlayer1Turn() {
        return player1Turn;
    }

}


