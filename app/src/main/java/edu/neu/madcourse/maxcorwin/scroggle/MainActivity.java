package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import edu.neu.madcourse.maxcorwin.R;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";
    private static final String EXISTING_GAME = "Existing Game";
    private static final String MUSIC_STRING = "Music";
    private static final String SOUNDS_STRING = "Sounds";
    private static final String NEW_GAME = "New Game";
    private static final String EMAIL_STRING = "Email";
    private static final String GAME_ID_LABEL = "Game ID";

    private MediaPlayer MEDIA_PLAYER;
    private CheckBox MUSIC_CHECKBOX, SOUNDS_CHECKBOX;

    private static final String COMMUNICATION = "Communication";
    private static final String TWO_PLAYER = "Two Player";
    private static final String LIVE = "Live";

    private boolean communication, twoPlayer;
    private Class gameType;
    private String gameId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw5_activity_main);

        MUSIC_CHECKBOX = (CheckBox) findViewById(R.id.music_checkbox);
        SOUNDS_CHECKBOX = (CheckBox) findViewById(R.id.sounds_checkbox);

        Bundle extras = getIntent().getExtras();
        communication = extras.getBoolean(COMMUNICATION);
        twoPlayer = extras.getBoolean(TWO_PLAYER);
        System.out.println(twoPlayer);
        if(communication) {
            findViewById(R.id.two_player_button).setVisibility(View.VISIBLE);
            TextView ackTextView = (TextView) findViewById(R.id.scroggle_ack_text_view);

            if(twoPlayer) {
                ackTextView.setText(getText(R.string.string_two_player_word_game_ack_text));
            }
            else {
                ackTextView.setText(getText(R.string.string_communication_ack_text));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeMediaPlayer();
        readFromSharedPreferences();
    }

    private void initializeMediaPlayer() {
        MEDIA_PLAYER = MediaPlayer.create(this, R.raw.ttt_menu_music);
        MEDIA_PLAYER.setOnPreparedListener(this);
        MEDIA_PLAYER.setVolume(0.5f, 0.5f);
        MEDIA_PLAYER.setLooping(true);
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        if(MUSIC_CHECKBOX.isChecked()) {
            MEDIA_PLAYER.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(MEDIA_PLAYER.isPlaying()) {
            MEDIA_PLAYER.stop();
        }
        MEDIA_PLAYER.release();

        writeToSharedPreferences();
    }

    private void readFromSharedPreferences() {
        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        MUSIC_CHECKBOX.setChecked(scroggleSettings.getBoolean(MUSIC_STRING, true));
        SOUNDS_CHECKBOX.setChecked(scroggleSettings.getBoolean(SOUNDS_STRING, true));

        boolean existingGame = scroggleSettings.getBoolean(EXISTING_GAME, false);

        if(scroggleSettings.getBoolean(TWO_PLAYER, false)) {
            gameId = scroggleSettings.getString(GAME_ID_LABEL, "");
            if(scroggleSettings.getBoolean(LIVE, false)) {
                gameType = TwoPlayerLiveGameActivity.class;
            }
            else {
                gameType = TwoPlayerUntimedGameActivity.class;
            }
        }
        else {
            gameType = GameActivity.class;
        }

        View continueGameButton = findViewById(R.id.continue_game_button);
        if(existingGame) {
            continueGameButton.setVisibility(View.VISIBLE);
        }
        else {
            continueGameButton.setVisibility(View.GONE);
        }
    }

    private void writeToSharedPreferences() {
        SharedPreferences scrogleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = scrogleSettings.edit();
        editor.putBoolean(MUSIC_STRING, MUSIC_CHECKBOX.isChecked());
        editor.putBoolean(SOUNDS_STRING, SOUNDS_CHECKBOX.isChecked());
        editor.apply();
    }

    public void musicAction(View v) {
        if(MUSIC_CHECKBOX.isChecked()) {
            try {
                MEDIA_PLAYER.prepareAsync();
            }
            catch (IllegalStateException e) {
                MEDIA_PLAYER.start();
            }
        }
        else if(MEDIA_PLAYER.isPlaying()) {
            MEDIA_PLAYER.stop();
        }
    }

    private void startGame(boolean newGame) {
        Intent intent = new Intent(this, gameType);
        intent.putExtra(NEW_GAME, newGame);
        intent.putExtra(GAME_ID_LABEL, gameId);
        writeToSharedPreferences();
        startActivity(intent);
    }

    public void newGameAction(View v) {
        startGame(true);
    }

    public void resumeGameAction(View v) {
        startGame(false);
    }

    public void twoPlayerAction(View v) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(TWO_PLAYER, twoPlayer);
        startActivity(intent);
    }

    public void showAckAction(View v) {
        (findViewById(R.id.scroggle_ack_layout)).setVisibility(View.VISIBLE);
    }

    public void hideAckAction(View v) {
        (findViewById(R.id.scroggle_ack_layout)).setVisibility(View.GONE);
    }

    public void quitAction(View v) {
        finish();
    }
}
