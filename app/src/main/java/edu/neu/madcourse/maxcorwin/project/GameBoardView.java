package edu.neu.madcourse.maxcorwin.project;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import edu.neu.madcourse.maxcorwin.R;

/**
 * Created by Max on 4/15/17.
 */

public class GameBoardView extends View {
    private Paint paint = new Paint();
    private Path path = new Path();
    private static final int SHAPES_PER_IMAGE = 100;
    private static final int MAX_RADIUS = 100;
    private int radius, color;
    final int STEP_SIZE = MAX_RADIUS/SHAPES_PER_IMAGE;
    private Level level;
    private static final int BACKGROUND_COLOR = 12566463;

    private static final int CIRCLE = 1;
    private static final int TRIANGLE = 2;
    private static final int SQUARE = 3;
    private static final int DIAMOND = 4;

    private float canvasXCenter, canvasYCenter;
    private float TWO = (float) 2.0;
    private float X_Touch, Y_Touch;
    private float left, right, top, bottom;
    private float leftX, leftY, topX, topY, rightX, rightY, bottomX, bottonY;

    private MediaPlayer imageSelectedPlayer, pairFoundPlayer, invalidPairPlayer, levelCompletePlayer;

    private Double[] doubleX, doubleY, doubleZ;
    private String[] timestamps;

    DatabaseReference db = FirebaseDatabase.getInstance().getReference();
    Random random = new Random();

    public GameBoardView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        imageSelectedPlayer = MediaPlayer.create(context, R.raw.sergenious_movex);
        pairFoundPlayer = MediaPlayer.create(context, R.raw.pair_found_sfx);
        invalidPairPlayer = MediaPlayer.create(context, R.raw.negative_sfx);
        levelCompletePlayer = MediaPlayer.create(context, R.raw.level_complete_sfx);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvasXCenter = getWidth()/TWO;
        canvasYCenter = getHeight()/TWO;

        setImageCenters();
        renderImages(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        X_Touch = event.getX();
        Y_Touch = event.getY();

        for(GameImage image : level.getImages()) {
            if(image.isOnBoard()
                    && !image.isSelected()
                    && level.getSelectedImages().size() < level.getPairSize()
                    && Math.abs(X_Touch - image.getXCenter()) <= MAX_RADIUS
                    && Math.abs(Y_Touch - image.getYCenter()) <= MAX_RADIUS)
            {
                System.out.println("Clicked!!!");
                selectImage(image);
            }
        }
        return true;
    }

    private void selectImage(GameImage image) {
        if(hasSound()) {
            imageSelectedPlayer.start();
        }
        image.setSelected(true);
        level.addToSelectedImages(image);

        ((FinalProjectGameActivity) getContext()).fillDot(level.getSelectedImages().size());
        checkForPairs();
    }

    private void checkForPairs() {
        if(level.getSelectedImages().size() == level.getPairSize()) {
            startTimerForSecondImageSelected();
        }
        invalidate();
    }

    private void pairFound() {
        System.out.println("Pair Found");

        for(GameImage selectedImage : level.getSelectedImages()) {
            selectedImage.setOnBoard(false);
        }

        level.clearSelectedImages();

        if(hasSound()) {
            pairFoundPlayer.start();
        }

        if(isLevelComplete()) {
            invalidate();
            startTimerForLevelComplete();
        }

        ((FinalProjectGameActivity) getContext()).initDots(level.getPairSize());

        invalidate();
    }

    private void mistakeMade() {
        System.out.println("Wrong!");
        recordMistake(level.getSelectedImages());

        if(hasSound()) {
            invalidPairPlayer.start();
        }

        level.clearSelectedImages();

        ((FinalProjectGameActivity) getContext()).initDots(level.getPairSize());

        invalidate();
    }

    private void recordMistake(ArrayList<GameImage> selectedImages) {
        DatabaseReference query = db.child("pairMistakes").child(String.valueOf(random.nextInt()));

        String childQuery;
        for(int i = 0; i < selectedImages.size(); i++) {
            childQuery = "timestamp" + String.valueOf(i);
            query.child(childQuery).setValue(selectedImages.get(i).getTimestamp());
        }
    }

    private boolean isLevelComplete() {
        for (GameImage image : level.getImages()) {
            if(!image.isUnique() && image.isOnBoard()) {
                return false;
            }
        }

        if(hasSound()) {
            levelCompletePlayer.start();
        }
        return true;
    }

    private void renderImages(Canvas canvas) {
        System.out.println(level.getImages().size() + " " + level.getNumImages());
        for(GameImage image : level.getImages()) {
            if(image.isOnBoard()) {
                if(image.isSelected() || level.getLevelNum() == 1) {
                    if(level.getLevelNum() == 1 && image.isSelected()) {
                        drawShape(canvas, image, MAX_RADIUS + 10, Color.BLACK);
                    }
                    for(int i = 0; i < SHAPES_PER_IMAGE; i++) {
                        radius = MAX_RADIUS - (STEP_SIZE * i);
                        color = image.getFullImage().get(i).getColor();
                        drawShape(canvas, image, radius, color);
                    }
                }
                else {
                    drawShape(canvas, image, MAX_RADIUS, Color.BLACK);
                }
            }
            else {
                drawShape(canvas, image, MAX_RADIUS, BACKGROUND_COLOR);
            }
        }
    }

    private void drawShape(Canvas canvas, GameImage image, int radius, int color) {
        paint.setColor(color);

        switch (image.getShapeType()) {
            case CIRCLE:
                drawCircle(canvas, image, radius);
                break;
            case TRIANGLE:
                drawTriangle(canvas, image, radius);
                break;
            case SQUARE:
                drawSquare(canvas, image, radius);
                break;
            case DIAMOND:
                drawDiamond(canvas, image, radius);
                break;
        }
    }

    private void drawCircle(Canvas canvas, GameImage image, int radius) {
        canvas.drawCircle(image.getXCenter(), image.getYCenter(), radius, paint);
    }

    private void drawTriangle(Canvas canvas, GameImage image, int radius) {
        leftX = image.getXCenter() - radius;
        leftY = image.getYCenter() + radius;
        topX = image.getXCenter();
        topY = image.getYCenter() - radius;
        rightX = image.getXCenter() + radius;
        rightY = image.getYCenter() + radius;


        path.reset();
        path.moveTo(leftX, leftY);
        path.lineTo(topX, topY);
        path.lineTo(rightX, rightY);
        path.lineTo(leftX, leftY);
        path.close();

        canvas.drawPath(path, paint);
    }

    private void drawSquare(Canvas canvas, GameImage image, int radius) {
        left = image.getXCenter() - radius;
        right = image.getXCenter() + radius;
        top = image.getYCenter() - radius;
        bottom = image.getYCenter() + radius;

        canvas.drawRect(left, top, right, bottom, paint);
    }

    private void drawDiamond(Canvas canvas, GameImage image, int radius) {
        leftX = image.getXCenter() - radius;
        leftY = image.getYCenter();
        topX = image.getXCenter();
        topY = image.getYCenter() - radius;
        rightX = image.getXCenter() + radius;
        rightY = image.getYCenter();
        bottomX = image.getXCenter();
        bottonY = image.getYCenter() + radius;


        path.reset();
        path.moveTo(leftX, leftY);
        path.lineTo(topX, topY);
        path.lineTo(rightX, rightY);
        path.lineTo(bottomX, bottonY);
        path.lineTo(leftX, leftY);
        path.close();

        canvas.drawPath(path, paint);
    }

    private void setImageCenters() {
        final int numImages = level.getNumImages();
        if(numImages == 2) {
            level.getImages().get(0).setXCenter(canvasXCenter);
            level.getImages().get(0).setYCenter(canvasYCenter/TWO);

            level.getImages().get(1).setXCenter(canvasXCenter);
            level.getImages().get(1).setYCenter(3*canvasYCenter/TWO);
        }
        else if(numImages == 3) {
            level.getImages().get(0).setXCenter(canvasXCenter);
            level.getImages().get(0).setYCenter(canvasYCenter/TWO);

            level.getImages().get(1).setXCenter(canvasXCenter);
            level.getImages().get(1).setYCenter(canvasYCenter);

            level.getImages().get(2).setXCenter(canvasXCenter);
            level.getImages().get(2).setYCenter(3*canvasYCenter/TWO);
        }
        else if(numImages == 4) {
            level.getImages().get(0).setXCenter(canvasXCenter/TWO);
            level.getImages().get(0).setYCenter(canvasYCenter/TWO);

            level.getImages().get(1).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(1).setYCenter(canvasYCenter/TWO);

            level.getImages().get(2).setXCenter(canvasXCenter/TWO);
            level.getImages().get(2).setYCenter(3*canvasYCenter/TWO);

            level.getImages().get(3).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(3).setYCenter(3*canvasYCenter/TWO);
        }
        else if(numImages == 9) {
            // Row 1
            level.getImages().get(0).setXCenter(canvasXCenter/TWO);
            level.getImages().get(0).setYCenter(canvasYCenter/TWO);

            level.getImages().get(1).setXCenter(canvasXCenter);
            level.getImages().get(1).setYCenter(canvasYCenter/TWO);

            level.getImages().get(2).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(2).setYCenter(canvasYCenter/TWO);

            // Row 2
            level.getImages().get(3).setXCenter(canvasXCenter/TWO);
            level.getImages().get(3).setYCenter(canvasYCenter);

            level.getImages().get(4).setXCenter(canvasXCenter);
            level.getImages().get(4).setYCenter(canvasYCenter);

            level.getImages().get(5).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(5).setYCenter(canvasYCenter);

            // Row 3
            level.getImages().get(6).setXCenter(canvasXCenter/TWO);
            level.getImages().get(6).setYCenter(3*canvasYCenter/TWO);

            level.getImages().get(7).setXCenter(canvasXCenter);
            level.getImages().get(7).setYCenter(3*canvasYCenter/TWO);

            level.getImages().get(8).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(8).setYCenter(3*canvasYCenter/TWO);
        }
        else {
            // Row 1
            level.getImages().get(0).setXCenter(canvasXCenter/TWO);
            level.getImages().get(0).setYCenter(3*canvasYCenter/7);

            level.getImages().get(1).setXCenter(canvasXCenter);
            level.getImages().get(1).setYCenter(3*canvasYCenter/7);

            level.getImages().get(2).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(2).setYCenter(3*canvasYCenter/7);

            // Row 2
            level.getImages().get(3).setXCenter(canvasXCenter/TWO);
            level.getImages().get(3).setYCenter(5*canvasYCenter/6);

            level.getImages().get(4).setXCenter(canvasXCenter);
            level.getImages().get(4).setYCenter(5*canvasYCenter/6);

            level.getImages().get(5).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(5).setYCenter(5*canvasYCenter/6);

            // Row 3
            level.getImages().get(6).setXCenter(canvasXCenter/TWO);
            level.getImages().get(6).setYCenter(5*canvasYCenter/4);

            level.getImages().get(7).setXCenter(canvasXCenter);
            level.getImages().get(7).setYCenter(5*canvasYCenter/4);

            level.getImages().get(8).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(8).setYCenter(5*canvasYCenter/4);

            // Row 4
            level.getImages().get(9).setXCenter(canvasXCenter/TWO);
            level.getImages().get(9).setYCenter(5*canvasYCenter/3);

            level.getImages().get(10).setXCenter(canvasXCenter);
            level.getImages().get(10).setYCenter(5*canvasYCenter/3);

            level.getImages().get(11).setXCenter(3*canvasXCenter/TWO);
            level.getImages().get(11).setYCenter(5*canvasYCenter/3);
        }
    }

    public void setLevel(int levelNum) {
        if(doubleX == null) {
            timestamps = ((FinalProjectGameActivity) getContext()).getTimestamps();
            doubleX = ((FinalProjectGameActivity) getContext()).getDoubleX();
            doubleY = ((FinalProjectGameActivity) getContext()).getDoubleY();
            doubleZ = ((FinalProjectGameActivity) getContext()).getDoubleZ();
        }

        level = new Level(levelNum, timestamps, doubleX, doubleY, doubleZ);
        ((FinalProjectGameActivity) getContext()).initDots(level.getPairSize());

        this.startTimerForImageFlip();
    }

    private void startTimerForSecondImageSelected() {
        final long IMAGE_SELECTED_DURATION = 1000;
        new CountDownTimer(IMAGE_SELECTED_DURATION, IMAGE_SELECTED_DURATION) {
            @Override
            public void onTick(long l) {
                // do nothing
            }

            @Override
            public void onFinish() {
                if(GameImage.allEqual(level.getSelectedImages())) {
                    // if the selected images make up a pair
                    pairFound();
                }
                else {
                    // selected images are not a pair
                    mistakeMade();
                }
            }
        }.start();
    }

    private void startTimerForLevelComplete() {
        final long LEVEL_COMPLETE_DURATION = 500;
        new CountDownTimer(LEVEL_COMPLETE_DURATION, LEVEL_COMPLETE_DURATION) {
            @Override
            public void onTick(long l) {
                // do nothing
            }

            @Override
            public void onFinish() {
                // TODO clear unmatched shapes (gray over)
                System.out.println("Level Complete");

                ((FinalProjectGameActivity) getContext()).incrementCurrentLevel();
                setLevel(((FinalProjectGameActivity) getContext()).getCurrentLevel());
                ((FinalProjectGameActivity) getContext()).initDots(level.getPairSize());

                invalidate();
            }
        }.start();
    }

    private void startTimerForImageFlip() {
        final long SHOW_IMAGE_DURATION = 3000;
        new CountDownTimer(SHOW_IMAGE_DURATION, SHOW_IMAGE_DURATION) {
            @Override
            public void onTick(long l) {
                // do nothing
            }

            @Override
            public void onFinish() {
                // make all images blacked out
                level.clearSelectedImages();
                invalidate();
            }
        }.start();
    }

    private boolean hasSound() {
        return ((FinalProjectGameActivity) getContext()).hasSound();
    }

    public void resetLevel() {
        level.reset();
        ((FinalProjectGameActivity) getContext()).initDots(level.getPairSize());
        startTimerForImageFlip();
        invalidate();
    }
}
