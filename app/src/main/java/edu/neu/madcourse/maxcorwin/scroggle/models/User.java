package edu.neu.madcourse.maxcorwin.scroggle.models;

import com.google.firebase.database.IgnoreExtraProperties;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * Created by Max on 3/1/17.
 */

@IgnoreExtraProperties
public class User {

    private String email;
    private String token;
    private long highScore;
    private String highScoreDate;
    private String friends;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

//    public User(String email, String token) {
//        this.email = email;
//        this.token = token;
//        updateHighScore(0);
//        this.friends = "";
//    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

    public long getHighScore() {
        return highScore;
    }

    public String getHighScoreDate() {
        return highScoreDate;
    }

//    public void updateHighScore(int score) {
//        this.highScore = score;
//        this.highScoreDate = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(new Date());
//    }

    public String getFriends() {
        return friends.trim();
    }


    public static ArrayList<String> makeFriendsList(String friends) {
        if(!friends.trim().isEmpty()) {
            return new ArrayList<>(Arrays.asList(friends.split("\\s+")));
        }
        return new ArrayList<>();
    }

    public static String makeFriendsString(ArrayList<String> friends) {
        String result = "";

        for(String s : friends) {
            result += s + " ";
        }
        return result;
    }
}
