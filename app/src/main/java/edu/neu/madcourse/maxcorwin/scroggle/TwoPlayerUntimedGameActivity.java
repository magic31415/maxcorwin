package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import edu.neu.madcourse.maxcorwin.R;
import edu.neu.madcourse.maxcorwin.scroggle.models.Game;
import edu.neu.madcourse.maxcorwin.scroggle.models.User;

public class TwoPlayerUntimedGameActivity extends AppCompatActivity {
    private static final String GAME_PREFS = "GamePreferenceFile";
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";

    private static final String EXISTING_GAME = "Existing Game";
    private static final String TWO_PLAYER = "Two Player";
    private static final String LIVE = "Live";

    private static final String CURRENT_BOGGLE_BOARD_ID = "Current Boggle Board Id";
    private static final String FROZEN_BOARDS_IDS = "Frozen Boards Ids";
    private static final String PREVIOUS_LETTER_LOCATION = "Previous Letter Location";
    private static final String WORD_TEXT = "Word Text";
    private static final String VISIBLE_BOARDS = "Visible Boards";

    private static final String GAME_ID_LABEL = "Game ID";

    private static final String MUSIC_STRING = "Music";
    private static final String SOUNDS_STRING = "Sounds";
    private static final String NEW_GAME = "New Game";
    private static final String EMAIL = "Email";

    private static final String TOO_SHORT_MSG = "Words must be at least three characters long.";
    private static final String NOT_WORD_MSG = "Not a valid word.";
    private static final String CANNOT_REUSE_MSG = "Cannot reuse words.";

    private boolean MUSIC, SOUNDS;
    private MediaPlayer MUSIC_PLAYER, SELECT_LETTER_PLAYER, FIND_WORD_PLAYER, WINNER_PLAYER, LOSER_PLAYER, TIE_PLAYER;

    private boolean existingGame;

    private ArrayList<String> dictionary = new ArrayList<>(); // excludes 9 letter words
    private ArrayList<String> nineLetterWords = new ArrayList<>();

    private final int NUMBER_OF_SEED_WORDS = 9;
    private final int FULL_WORD_LENGTH = 9;

    private TextView WORD, MY_EMAIL, OTHER_EMAIL, MY_SCORE, OTHER_SCORE;
    private Button CLEAR_BUTTON, SUBMIT_BUTTON;
    private int GREEN_COLOR, RED_COLOR, BROWN_COLOR, WHITE_COLOR;

    private final ArrayList<Integer> TILES = new ArrayList<>(Arrays.asList(
            R.id.small1, R.id.small2, R.id.small3, R.id.small4, R.id.small5,
            R.id.small6, R.id.small7, R.id.small8, R.id.small9));

    private final ArrayList<Integer> LARGE_BOARDS = new ArrayList<>(Arrays.asList(
            R.id.large1, R.id.large2, R.id.large3, R.id.large4, R.id.large5,
            R.id.large6, R.id.large7, R.id.large8, R.id.large9));

    private final HashMap<Integer, ArrayList<Character>> SCORE_MAP = initScoreMap();
    private final ArrayList<Integer> POINT_AMOUNTS = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 8, 10));

    private ViewParent currentBoggleBoard;
    private int currentBoggleBoardId;
    private ArrayList<ViewParent> frozenBoards = new ArrayList<>();
    private ArrayList<String> frozenBoardIds = new ArrayList<>();


    private int previousLetterLocation;
    private String wordText;

    private DatabaseReference db;
    private ValueEventListener listener;
    private String gameId, myEmail, otherEmail, boardWords, boardColors, wordsFound;
    private String player1FrozenBoards = "", player2FrozenBoards = "";
    private int myScore, otherScore;
    private boolean isPlayer1, partTwo, player1Done, player2Done;

    private ArrayList<String> visibleBoards = new ArrayList<>();
    private boolean player1Turn;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw8_activity_two_player_live_game);
        initializeVariables();

        if(getIntent().getExtras() != null && getIntent().getExtras().getBoolean(NEW_GAME, true)) {
            clearGamePreferences();
        }
        else {
            readFromSharedPreferences();
            readDataFromFirebase();
        }

        // Creates dictionary and seeds board
        new TwoPlayerUntimedGameActivity.DictionaryCreator(getResources()).execute();

        hideAllBoards();
        revealVisibleBoards();
    }

    private void hideAllBoards() {
        for(int board : LARGE_BOARDS) {
            findViewById(board).setVisibility(View.INVISIBLE);
        }
    }

    private void initializeVariables() {

        MUSIC_PLAYER = MediaPlayer.create(this, R.raw.ttt_game_music);
        SELECT_LETTER_PLAYER = MediaPlayer.create(this, R.raw.sergenious_movex);
        FIND_WORD_PLAYER = MediaPlayer.create(this, R.raw.beep);
        WINNER_PLAYER = MediaPlayer.create(this, R.raw.oldedgar_winner);
        LOSER_PLAYER = MediaPlayer.create(this, R.raw.notr_loser);
        TIE_PLAYER = MediaPlayer.create(this, R.raw.department64_draw);

        GREEN_COLOR = getResources().getColor(R.color.green_scroggle);
        RED_COLOR = getResources().getColor(R.color.red_scroggle);
        BROWN_COLOR = getResources().getColor(R.color.brown_scroggle);
        WHITE_COLOR = getResources().getColor(R.color.white);

        CLEAR_BUTTON = (Button) findViewById(R.id.button_clear);
        SUBMIT_BUTTON = (Button) findViewById(R.id.button_submit);
        WORD = (TextView) findViewById(R.id.word);

        MY_EMAIL = (TextView) findViewById(R.id.my_email);
        OTHER_EMAIL = (TextView) findViewById(R.id.other_email);
        MY_SCORE = (TextView) findViewById(R.id.my_score);
        OTHER_SCORE = (TextView) findViewById(R.id.other_score);

        listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Game game = dataSnapshot.getValue(Game.class);

                if(!partTwo) {
                    boardColors = game.getBoardColors();

                    if(isPlayer1) {
                        otherScore = game.getPlayer2Score();
                    }
                    else {
                        otherScore = game.getPlayer1Score();
                    }
                    OTHER_SCORE.setText(String.valueOf(otherScore));
                }

                boardWords = game.getBoardWords();
                wordsFound = game.getWordsFound();
                player1Done = game.getPlayer1Done();
                player2Done = game.getPlayer2Done();

                unpackBoardColors();
                System.out.println(boardColors);

                if(isMyTurn(game.isPlayer1Turn())) {
                    if(frozenBoards.size() == NUMBER_OF_SEED_WORDS) {
                        startPartTwo();
                    }
                    else {
                        revealNextBoard();
                    }
                }

                player1Turn = game.isPlayer1Turn();
                player1FrozenBoards = game.getPlayer1FrozenBoards();
                player2FrozenBoards = game.getPlayer2FrozenBoards();

                if(player1Done && player2Done) {

                    if(isPlayer1) {
                        otherScore = game.getPlayer2Score();
                    }
                    else {
                        otherScore = game.getPlayer1Score();
                    }
                    OTHER_SCORE.setText(String.valueOf(otherScore));

                    setFinalBoardColors(game.getBoardColors());
                    unpackBoardColors();
                    gameOver();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private boolean isMyTurn(boolean newPlayer1Turn) {
        if(player1Turn != newPlayer1Turn) {
            return isPlayer1 == newPlayer1Turn;
        }
        return false;
    }

    private void setFinalBoardColors(String otherBoardColors) { // TODO not working perfectly
        String newBoardColors = "";

        for(int i = 0; i < boardColors.length(); i++) {
            if(boardColors.charAt(i) == '4' || boardColors.charAt(i) == '0') {
                newBoardColors += otherBoardColors.charAt(i);
            }
            else if(otherBoardColors.charAt(i) == '4' || otherBoardColors.charAt(i) == '0') {
                newBoardColors += boardColors.charAt(i);
            }
            else if(otherBoardColors.charAt(i) == '1' && boardColors.charAt(i) == '1') {
                newBoardColors += '1';
            }
            else if(otherBoardColors.charAt(i) == '2' && boardColors.charAt(i) == '2') {
                newBoardColors += '2';
            }
            else {
                newBoardColors += "3";
            }
        }
        boardColors = newBoardColors;
        writeDataToFirebase("boardColors", boardColors);
        unpackBoardColors();
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("On Start");
        readFromSharedPreferences();
        readDataFromFirebase();
        existingGame = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("On Resume");
        playMusic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("On Pause");
        stopMusic();
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("On Stop");
        writeToSharedPreferences();
    }

    private void readFromSharedPreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        currentBoggleBoardId = gameState.getInt(CURRENT_BOGGLE_BOARD_ID, -1);
        currentBoggleBoard = (ViewParent) findViewById(currentBoggleBoardId);
        frozenBoardIds = new ArrayList<>(gameState.getStringSet(FROZEN_BOARDS_IDS, new HashSet<String>()));
        for(String id : frozenBoardIds) {
            frozenBoards.add((ViewParent) findViewById(Integer.valueOf(id)));
        }
        previousLetterLocation = gameState.getInt(PREVIOUS_LETTER_LOCATION, -1);
        wordText = gameState.getString(WORD_TEXT, "");
        visibleBoards = new ArrayList<>(gameState.getStringSet(VISIBLE_BOARDS, new HashSet<String>()));


        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        MUSIC = scroggleSettings.getBoolean(MUSIC_STRING, true);
        SOUNDS = scroggleSettings.getBoolean(SOUNDS_STRING, true);
        existingGame = scroggleSettings.getBoolean(EXISTING_GAME, true);
        myEmail = toPeriods(scroggleSettings.getString(EMAIL, ""));
    }

    private void writeToSharedPreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = gameState.edit();
        editor.putInt(CURRENT_BOGGLE_BOARD_ID, currentBoggleBoardId);
        editor.putStringSet(FROZEN_BOARDS_IDS, new HashSet<>(frozenBoardIds));
        editor.putInt(PREVIOUS_LETTER_LOCATION, previousLetterLocation);
        editor.putString(WORD_TEXT, wordText);
        editor.putStringSet(VISIBLE_BOARDS, new HashSet<>(visibleBoards));
        editor.apply();

        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        editor = scroggleSettings.edit();
        editor.putBoolean(EXISTING_GAME, existingGame);
        editor.putBoolean(TWO_PLAYER, true);
        editor.putBoolean(LIVE, false);
        editor.putString(GAME_ID_LABEL, gameId);
        editor.apply();
    }

    private void readDataFromFirebase() {
        db = FirebaseDatabase.getInstance().getReference();
        gameId = getIntent().getExtras().getString(GAME_ID_LABEL);

        getGame().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Game game = dataSnapshot.getValue(Game.class);

                    isPlayer1 = game.getPlayer1Email().equals(myEmail);

                    if(isPlayer1) {
                        otherEmail = game.getPlayer2Email();
                        myScore = game.getPlayer1Score();
                        otherScore = game.getPlayer2Score();

                        if(visibleBoards.isEmpty()) {
                            revealNextBoard();
                        }
                    }
                    else {
                        otherEmail = game.getPlayer1Email();
                        myScore = game.getPlayer2Score();
                        otherScore = game.getPlayer1Score();
                    }

                    if(otherScore != 0 && (isPlayer1 && (!player2FrozenBoards.equals(game.getPlayer2FrozenBoards())) ||
                                    (!isPlayer1 && (!player1FrozenBoards.equals(game.getPlayer1FrozenBoards())))))
                    {
                        if(frozenBoards.size() == NUMBER_OF_SEED_WORDS) {
                            startPartTwo();
                        }
                        else {
                            revealNextBoard();
                        }
                    }

                    boardWords = game.getBoardWords();
                    boardColors = game.getBoardColors();
                    partTwo = game.isPartTwo();
                    wordsFound = game.getWordsFound();

                    player1FrozenBoards = game.getPlayer1FrozenBoards();
                    player2FrozenBoards = game.getPlayer2FrozenBoards();
                    player1Done = game.getPlayer1Done();
                    player2Done = game.getPlayer2Done();
                    player1Turn = game.isPlayer1Turn();

                    MY_EMAIL.setText(myEmail);
                    OTHER_EMAIL.setText(otherEmail);
                    MY_SCORE.setText(String.valueOf(myScore));
                    OTHER_SCORE.setText(String.valueOf(otherScore));
                }
                catch (NullPointerException e) {
                    System.out.println("NO GAME!!!!!!!!!!!");
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        getGame().addValueEventListener(listener);
    }

    private void writeDataToFirebase(String key, Object value) {
        // TODO Check for internet
        getGame().child(key).setValue(value);
    }

    private DatabaseReference getGame() {
        return db.child("games").child(gameId);
    }

    private void clearGamePreferences() {
        SharedPreferences gameState = this.getApplicationContext().getSharedPreferences(GAME_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = gameState.edit();
        editor.clear();
        editor.apply();
    }

    private void playMusic() {
        if(MUSIC) {
            MUSIC_PLAYER.setVolume(0.5f, 0.5f);
            MUSIC_PLAYER.setLooping(true);
            MUSIC_PLAYER.start();
        }
    }

    private void playSound(MediaPlayer mediaPlayer) {
        if(SOUNDS) {
            mediaPlayer.setVolume(1.0f, 1.0f);
            mediaPlayer.start();
        }
    }

    private void stopMusic() {
        if(MUSIC) {
            MUSIC_PLAYER.stop();
            MUSIC_PLAYER.release();
        }
    }

    private void startPartTwo() {
        partTwo = true;
        writeDataToFirebase("partTwo", true);
        WORD.setAllCaps(false);
        WORD.setText(R.string.part_2_instructions);

        emptyFrozenBoards();
        writeDataToFirebase("player1FrozenBoards", " ");
        writeDataToFirebase("player2FrozenBoards", " ");

        boardColors = boardColors.replaceAll("0", "4").replaceAll("1", "0").replaceAll("2", "0").replaceAll("3", "0");
        //writeDataToFirebase("boardColors", boardColors);

        clearAllBoards();
        CLEAR_BUTTON.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAllBoards();
                WORD.setText("");
            }
        });

        Button submitButton = (Button) findViewById(R.id.button_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitPartTwo();
            }
        });
    }

    private void clearAllBoards() {
        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            for(int board : LARGE_BOARDS) {
                currentBoggleBoard = (ViewParent) findViewById(board);
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                clearCurrentBoard();
            }
        }
    }

    public void selectLetterAction(View v) {
        final Button tile = (Button) v;
        final ViewParent thisBoggleBoard = tile.getParent();

        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_1_instructions).toString())
                || word.equalsIgnoreCase(getText(R.string.part_2_instructions).toString()) ) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        if(partTwo) {
            selectLetterPartTwo(v);
        }
        else if(!frozenBoards.contains(thisBoggleBoard)) {
            if(currentBoggleBoard == null) {
                currentBoggleBoard = thisBoggleBoard;
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
            else if(currentBoggleBoard.equals(thisBoggleBoard)) {
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
            else {
                clearAction(v);
                currentBoggleBoard = thisBoggleBoard;
                currentBoggleBoardId = ((View) currentBoggleBoard).getId();
                if(isValidNextLetter(tile)) {
                    addLetter(tile);
                }
            }
        }
    }

    private void selectLetterPartTwo(View view) {
        final Button tile = (Button) view;
        int tileColor = ((ColorDrawable) tile.getBackground()).getColor();

        if(!(tile.getText().equals("")
                || tileColor == GREEN_COLOR
                || frozenBoards.size() == NUMBER_OF_SEED_WORDS)) {
            addLetter(tile);
        }
    }

    private boolean isValidNextLetter(Button tile) {
        int nextLetterLocation = Integer.valueOf(tile.getTag().toString());
        int tileColor = ((ColorDrawable) tile.getBackground()).getColor();

        if((previousLetterLocation == -1
                || initNeighbors().get(previousLetterLocation).contains(nextLetterLocation))
                && (tileColor != GREEN_COLOR) && (tileColor != BROWN_COLOR)) {
            previousLetterLocation = nextLetterLocation;
            return true;
        }
        return false;
    }

    private void addLetter(Button tile) {
        int color = ((ColorDrawable) tile.getBackground()).getColor();
        String replacementCharacter;
        if(color == WHITE_COLOR) {
            if(isPlayer1) {
                replacementCharacter = "1";
            }
            else {
                replacementCharacter = "2";
            }
        }
        else {
            replacementCharacter = "3";
        }

        recolorTile(getTileNumber(tile), replacementCharacter);

        if(partTwo) {
            unpackBoardColors();
        }
        else {
            writeDataToFirebase("boardColors", boardColors);
        }

        playSound(SELECT_LETTER_PLAYER);
        String newWord = formatTextViewString(WORD) + tile.getText().toString().toUpperCase();
        WORD.setText(newWord);
    }

    private void clearCurrentBoard() {
        ViewGroup smallBoard = ((ViewGroup) currentBoggleBoard);
        View tile;

        for(int i = 0; i < FULL_WORD_LENGTH; i++) {
            tile = smallBoard.getChildAt(i);

            restoreTileColor(tile);

            if(partTwo) {
                unpackBoardColors();
            }
            else {
                writeDataToFirebase("boardColors", boardColors);
            }
        }
    }

    private void restoreTileColor(View tile) {
        int tileNumber = getTileNumber(tile);

        switch (boardColors.charAt(tileNumber)) {
            case '1':
                if (isPlayer1) {
                    recolorTile(tileNumber, "0");
                }
                break;

            case '2':
                if (!isPlayer1) {
                    recolorTile(tileNumber, "0");
                }
                break;

            case '3':
                if (isPlayer1) {
                    recolorTile(tileNumber, "2");
                } else {
                    recolorTile(tileNumber, "1");
                }
                break;
        }
    }

    private int getTileNumber(View tile) {
        ViewGroup smallBoard = (ViewGroup) tile.getParent();
        int boardNumber = getBoardNumber(smallBoard);

        return (boardNumber * FULL_WORD_LENGTH)
                + (Integer.valueOf(tile.getTag().toString()) - 1);
    }

    private int getBoardNumber(ViewGroup smallBoard) {
        int boardNumber = -1;
        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            if(((ViewGroup) smallBoard.getParent()).getChildAt(i) == smallBoard) {
                boardNumber = i;
                break;
            }
        }
        return boardNumber;
    }

    private void recolorTile(int index, String character) {
        boardColors = boardColors.substring(0, index) + character + boardColors.substring(index + 1);
    }

    private void freezeBoard() {
        frozenBoards.add(currentBoggleBoard);
        frozenBoardIds.add(Integer.toString(((View) currentBoggleBoard).getId()));

        if(isPlayer1) {
            player1FrozenBoards += getBoardNumber((ViewGroup) currentBoggleBoard) + " ";
            writeDataToFirebase("player1FrozenBoards", player1FrozenBoards);
        }
        else {
            player2FrozenBoards += getBoardNumber((ViewGroup) currentBoggleBoard) + " ";
            writeDataToFirebase("player2FrozenBoards", player2FrozenBoards);
        }

        calculateBoardColors();

        currentBoggleBoard = null;
        currentBoggleBoardId = -1;
        previousLetterLocation = -1;
    }

    private void calculateBoardColors() {
        int boardNumber = getBoardNumber((ViewGroup) currentBoggleBoard);
        String boardNumberString = " " + boardNumber + " ";

        int startIdx = boardNumber * FULL_WORD_LENGTH;
        String thisBoardColors = boardColors.substring(startIdx, startIdx + FULL_WORD_LENGTH);
        boolean boardCompletelyFrozen =
                player1FrozenBoards.contains(boardNumberString) && player2FrozenBoards.contains(boardNumberString);

        if(boardCompletelyFrozen) {
            String newBoardColors = thisBoardColors.replaceAll("0", "4");
            boardColors = boardColors.substring(0, startIdx) + newBoardColors + boardColors.substring(startIdx + FULL_WORD_LENGTH);
            writeDataToFirebase("boardColors", boardColors);
        }
    }

    private void freezeAllBoards() {
        for(int boardId : LARGE_BOARDS) {
            currentBoggleBoard =  (ViewParent) findViewById(boardId);
            currentBoggleBoardId = ((View) currentBoggleBoard).getId();

            if(!frozenBoards.contains(currentBoggleBoard)) {
                freezeBoard();
            }
        }
    }

    public void submitAction(View v) {
        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_1_instructions).toString())) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        String foundWord = formatTextViewString(WORD).toLowerCase();

        if(foundWord.isEmpty()) {
            // Do Nothing
        }
        else if(foundWord.length() < 3) {
            showToast(TOO_SHORT_MSG);
        }
        else if (wordsFound.contains(" " + foundWord + " ")) {
            showToast(CANNOT_REUSE_MSG);
        }
        else if(isValidWord(foundWord)) {
            wordsFound += foundWord + " ";
            writeDataToFirebase("wordsFound", wordsFound);
            playSound(FIND_WORD_PLAYER);
            freezeBoard();

            myScore += calculateWordScore();
            updateScoreInFirebase();
            MY_SCORE.setText(String.valueOf(myScore));

            WORD.setText("");
//            if(frozenBoards.size() == NUMBER_OF_SEED_WORDS) {
//                updateScoreInFirebase();
////                endGameEarly();
//            }
        }
        else {
            showToast(NOT_WORD_MSG);
        }
    }

    private void submitPartTwo() {
        final String word = formatTextViewString(WORD);
        if(word.equalsIgnoreCase(getText(R.string.part_2_instructions).toString()) ) {
            WORD.setText("");
            WORD.setAllCaps(true);
        }

        String foundWord = formatTextViewString(WORD).toLowerCase();

        if(foundWord.isEmpty()) {
            // Do Nothing
        }
        else if(foundWord.length() < 3) {
            showToast(TOO_SHORT_MSG);
        }
        else if(wordsFound.contains(" " + foundWord + " ")) {
            showToast(CANNOT_REUSE_MSG);
        }
        else if(isValidWord(foundWord)) {
            playSound(FIND_WORD_PLAYER);
            freezeAllBoards();

            myScore += calculateWordScore();
            updateScoreInFirebase();
            MY_SCORE.setText(String.valueOf(myScore));

            WORD.setText("");

            if(isPlayer1) {
                writeDataToFirebase("player1Done", true);
            }
            else {
                writeDataToFirebase("player2Done", true);
            }
        }
        else {
            showToast(NOT_WORD_MSG);
        }
    }

    private void updateScoreInFirebase() {
        if(isPlayer1) {
            writeDataToFirebase("player1Score", myScore);
        }
        else {
            writeDataToFirebase("player2Score", myScore);
        }

        writeDataToFirebase("player1Turn", !player1Turn);

        PushNotification pushNotification = new PushNotification();
        retrieveToken();
        System.out.println("Token " + token);
        pushNotification.sendNotification(token);
    }

    private void retrieveToken() {
        db.child("users").child(toCommas(otherEmail)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                token = user.getToken();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void clearAction(View v) {
        clearSmallBoard();
    }

    private void clearSmallBoard() {
        previousLetterLocation = -1;
        WORD.setText("");
        if(currentBoggleBoard != null) {
            clearCurrentBoard();
        }
    }

    private boolean isValidWord(String foundWord) {
        return dictionary.contains(foundWord) || nineLetterWords.contains(foundWord);
    }

    private int calculateWordScore() {
        int wordScore = 0;
        String word = formatTextViewString(WORD);

        for(int i = 0; i < word.length(); i++) {
            for(int amount : POINT_AMOUNTS) {
                if(SCORE_MAP.get(amount).contains(word.charAt(i))) {
                    wordScore += amount;
                    break;
                }
            }
        }
        return wordScore;
    }

    private HashMap<Integer, ArrayList<Character>> initScoreMap() {
        HashMap<Integer, ArrayList<Character>> scoreMap = new HashMap<>();
        scoreMap.put(1, new ArrayList<>(Arrays.asList('E', 'A', 'I', 'O', 'N', 'R', 'T', 'L', 'S', 'U')));
        scoreMap.put(2, new ArrayList<>(Arrays.asList('D', 'G')));
        scoreMap.put(3, new ArrayList<>(Arrays.asList('B', 'C', 'M', 'P')));
        scoreMap.put(4, new ArrayList<>(Arrays.asList('F', 'H','V', 'W', 'Y')));
        scoreMap.put(5, new ArrayList<>(Collections.singletonList('K')));
        scoreMap.put(8, new ArrayList<>(Arrays.asList('J', 'X')));
        scoreMap.put(10, new ArrayList<>(Arrays.asList('Q', 'Z')));

        return scoreMap;
    }

    private HashMap<Integer, ArrayList<Integer>> initNeighbors() {
        HashMap<Integer, ArrayList<Integer>> neighbors = new HashMap<>();
        neighbors.put(1, new ArrayList<>(Arrays.asList(2, 4, 5)));
        neighbors.put(2, new ArrayList<>(Arrays.asList(1, 3, 4, 5, 6)));
        neighbors.put(3, new ArrayList<>(Arrays.asList(2, 5, 6)));
        neighbors.put(4, new ArrayList<>(Arrays.asList(1, 2, 5, 7, 8)));
        neighbors.put(5, new ArrayList<>(Arrays.asList(1, 2, 3, 4, 6, 7, 8, 9)));
        neighbors.put(6, new ArrayList<>(Arrays.asList(2, 3, 5, 8, 9)));
        neighbors.put(7, new ArrayList<>(Arrays.asList(4, 5, 8)));
        neighbors.put(8, new ArrayList<>(Arrays.asList(4, 5, 6, 7, 9)));
        neighbors.put(9, new ArrayList<>(Arrays.asList(5, 6, 8)));

        return neighbors;
    }

    private String formatTextViewString(TextView view) {
        return view.getText().toString().toUpperCase();
    }

    private void showToast(String message) {
        Toast.makeText(TwoPlayerUntimedGameActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void hideClearSubmit() {
        CLEAR_BUTTON.setVisibility(View.INVISIBLE);
        SUBMIT_BUTTON.setVisibility(View.INVISIBLE);
    }

    private void emptyFrozenBoards() {
        frozenBoards.clear();
        frozenBoardIds.clear();
    }

    public void quitAction(View view) {
        writeToSharedPreferences();
        finish();
    }

    private class DictionaryCreator extends AsyncTask<Void, Void, Void> {
        private Resources resources;
        private final TextView LOADING_SCREEN = (TextView) findViewById(R.id.scroggle2_loading_screen);

        DictionaryCreator(Resources resources) {
            this.resources = resources;
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("Start loading dictionary");
            readInDictionaryWords(resources);
            System.out.println(gameId);
            System.out.println("End loading dictionary");
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LOADING_SCREEN.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LOADING_SCREEN.setVisibility(View.GONE);

            try {
                seedBoard();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void readInDictionaryWords(Resources resources) {
            String word;

            try {
                InputStream inputStream = resources.openRawResource(R.raw.wordlist);
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);

                while ((word = bufferedReader.readLine()) != null) {
                    word = word.trim();
                    if (word.length() == FULL_WORD_LENGTH) {
                        nineLetterWords.add(word);
                    }
                    else {
                        dictionary.add(word);
                    }
                }
                bufferedReader.close();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void seedBoard() throws InterruptedException {

        // create new words
        if(boardWords.isEmpty()) {
            if(isPlayer1) {
                WordPicker wordPicker = new WordPicker(nineLetterWords);
                boardWords = wordPicker.getWordsAsString();
                writeDataToFirebase("boardWords", boardWords);
                redrawBoard();
            }
            else {
                Thread.sleep(1000); // TODO what's going on here
                getGame().child("boardWords").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boardWords = dataSnapshot.getValue().toString(); // TODO what about onresume
                        redrawBoard();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
        else {
            redrawBoard();
        }
    }

    private void redrawBoard() {
        ArrayList<String> seedWords = new ArrayList<>();

        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            int startIndex = i * FULL_WORD_LENGTH;
            int endIndex = startIndex + FULL_WORD_LENGTH;
            seedWords.add(boardWords.substring(startIndex, endIndex));
        }

        System.out.println(boardWords);
        for(int i = 0; i < seedWords.size(); i++) {
            addToBoard(seedWords.get(i), LARGE_BOARDS.get(i));
        }

        unpackBoardColors(); // TODO needed?
//        emptyFrozenBoards(); // TODO ??
    }

    private void addToBoard(String word, int largeBoardId) {
        for(int i = 0; i < word.length(); i++) {
            Button tile = getTile(TILES.get(i), largeBoardId);
            String letter = word.substring(i, i + 1);
            tile.setText(letter);
        }
    }

    private Button getTile(int tile, int largeBoardId) {
        return (Button) findViewById(largeBoardId).findViewById(tile);
    }

    private void unpackBoardColors() {
        for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
            ViewGroup smallBoard = (ViewGroup) findViewById(LARGE_BOARDS.get(i));
            View tile;
            int index;

            for(int j = 0; j < FULL_WORD_LENGTH; j++) {
                tile = smallBoard.getChildAt(j);
                index = i * FULL_WORD_LENGTH + j;

                int color;
                switch(boardColors.charAt(index)) {
                    case '0':
                        color = WHITE_COLOR;
                        break;
                    case '1':
                        if (isPlayer1) {
                            color = GREEN_COLOR;
                        } else {
                            color = RED_COLOR;
                        }
                        break;
                    case '2':
                        if (isPlayer1) {
                            color = RED_COLOR;
                        } else {
                            color = GREEN_COLOR;
                        }
                        break;
                    case '3':
                        color = BROWN_COLOR;
                        break;
                    default: // 4
                        color = WHITE_COLOR;
                        ((Button) tile).setText("");
                        break;
                }
                tile.setBackgroundColor(color);
            }
        }
    }

    private void gameOver() {
        int message;
        MediaPlayer player;

        if(myScore > otherScore) {
            message = R.string.string_winner;
            player = WINNER_PLAYER;
        }
        else if(otherScore > myScore) {
            message = R.string.string_loser;
            player = LOSER_PLAYER;
        }
        else {
            message = R.string.string_tie;
            player = TIE_PLAYER;
        }

        WORD.setAllCaps(true);
        WORD.setText(message);
        playSound(player);
        existingGame = false;
        hideClearSubmit();
        getGame().removeEventListener(listener);
        // TODO remove other listener


        db.child("users").child(toCommas(myEmail)).child("highScore").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long oldHighScore = (Long) dataSnapshot.getValue();
                long thisScore = myScore * -1;
                String date = new SimpleDateFormat("MM-dd-yyyy HH:mm").format(new Date());

                if(thisScore < oldHighScore) {
                    db.child("users").child(toCommas(myEmail)).child("highScore").setValue(thisScore);
                    db.child("users").child(toCommas(myEmail)).child("highScoreDate").setValue(date);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void revealNextBoard() {
        final int setSize = visibleBoards.size();
        int boardNumber = -1;

        while(visibleBoards.size() == setSize) {
            boardNumber = new Random().nextInt(NUMBER_OF_SEED_WORDS);
            visibleBoards.add(String.valueOf(boardNumber));
        }
        findViewById(LARGE_BOARDS.get(boardNumber)).setVisibility(View.VISIBLE);
    }

    private void revealVisibleBoards() {
        for(String boardNumber : visibleBoards) {
            findViewById(LARGE_BOARDS.get(Integer.valueOf(boardNumber))).setVisibility(View.VISIBLE);
        }
    }

    private String toPeriods(String email) {
        return email.replaceAll(",", ".");
    }

    private String toCommas(String email) {
        return email.replaceAll("\\.", ",");
    }
}
