package edu.neu.madcourse.maxcorwin;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;

public class DictionaryActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "DictionaryPreferenceFile";
    public static final String WL_STRING = "WordListString";

    ArrayList<String> dictionary = new ArrayList<>();
    ArrayList<String> printedWordsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw3_activity_dictionary);
        TextView wordListView = (TextView) findViewById(R.id.word_list);
        wordListView.setMovementMethod(new ScrollingMovementMethod());

        createDictionary();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String wordListString = settings.getString(WL_STRING, "").trim();

        TextView wordListView = (TextView) findViewById(R.id.word_list);

        wordListView.setText(wordListString);
        System.out.println(wordListString);
        if(!wordListString.isEmpty()) {
            wordListView.append("\n");
        }

        printedWordsList = split(wordListString + "\n");
    }

    @Override
    protected void onStop() {
        super.onStop();
        TextView wordListView = (TextView) findViewById(R.id.word_list);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(WL_STRING, wordListView.getText().toString());
        editor.apply();
    }

    private static ArrayList<String> split(String wordListString) {
        return new ArrayList<>(Arrays.asList(wordListString.split("\n")));
    }

    public void mainAction(View v) {
//        startActivity(new Intent(this, MainActivity.class));
        finish();
        System.exit(0);
    }

    public void ackAction(View v) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ack_layout);
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void hideAckAction(View v) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ack_layout);
        linearLayout.setVisibility(View.GONE);
    }

    public void wordsAction(View v) {
        final MediaPlayer beepSound = MediaPlayer.create(this, R.raw.beep);
        beepSound.setVolume(0.5f, 0.5f);

        EditText textbox = (EditText) findViewById(R.id.textbox);

        textbox.addTextChangedListener(new TextWatcher() {
            TextView wordListView = (TextView) findViewById(R.id.word_list);

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String word = s.toString().toLowerCase();

                if (word.length() >= 3 && !printedWordsList.contains(word) && dictionary.contains(word)) {
                    wordListView.append(word + "\n");
                    printedWordsList.add(word);
                    beepSound.start();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable editable) { }

        });
    }

    public void clearTextboxAction(View v) {
        EditText textbox = (EditText) findViewById(R.id.textbox);
        textbox.setText("");
    }

    public void clearAllAction(View v) {
        clearTextboxAction(v);

        TextView wordListView = (TextView) findViewById(R.id.word_list);
        wordListView.setText("");
        printedWordsList.clear();
    }

    public void createDictionary() {
        new DictionaryCreator(getResources()).execute();
    }

    private class DictionaryCreator extends AsyncTask<Void, Void, Void> {
        private Resources resources;

        DictionaryCreator(Resources resources) {
            this.resources = resources;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String word;

            try {
                InputStream inputStream = resources.openRawResource(R.raw.wordlist);
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);

                while((word = bufferedReader.readLine()) != null) {
                    word = word.trim();
                    dictionary.add(word);
                }
                bufferedReader.close();
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.println("Starting");
            TextView loadingScreen = (TextView) findViewById(R.id.loading_screen);
            loadingScreen.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            System.out.println("Ending");
            TextView loadingScreen = (TextView) findViewById(R.id.loading_screen);
            loadingScreen.setVisibility(View.GONE);
        }
    }
}