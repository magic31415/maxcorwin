package edu.neu.madcourse.maxcorwin.project;

import android.graphics.Color;

/**
 * Created by jzstern on 4/17/17.
 */

public class GameShape {
    private int color;

    private final double MAXIMUM_ACCELERATION = 2.5;
    private final int MAXIMUM_COLOR_VALUE = 255;
    private final double RED_BOOST = 1.68;
    private final double GREEN_BOOST = 1.2;
    private final double BLUE_BOOST = 1.56;

    public GameShape(double x, double y, double z) {
        this.setColor(x, y, z);
    }

    private void setColor(double x, double y, double z) {
        int red, green, blue;

        red = calculateColorComponent(x * RED_BOOST);
        green = calculateColorComponent(y * GREEN_BOOST);
        blue = calculateColorComponent(z * BLUE_BOOST);
        color = Color.rgb(red, green, blue);
    }

    private int calculateColorComponent(double accel) {
        return (int) Math.round((Math.abs(accel) / MAXIMUM_ACCELERATION) * MAXIMUM_COLOR_VALUE);
    }

    public int getColor() {
        return color;
    }
}
