package edu.neu.madcourse.maxcorwin.scroggle;

import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import edu.neu.madcourse.maxcorwin.R;

/**
 * Created by Max on 3/15/17.
 */

public class WordPicker {
    /* Constants */
    private static final int NUMBER_OF_SEED_WORDS = 9;
    private static final int FULL_WORD_LENGTH = 9;
    private final List<Integer> ORIGINAL_ORDER = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8);
    private final HashMap<Integer, ArrayList<Integer>> NEIGHBORS = initNeighbors();

    /* Class Variables */
    private String wordsAsString = "";
    private ArrayList<String> nineLetterWords;

    public WordPicker(ArrayList<String> nineLetterWords) {
        this.nineLetterWords = nineLetterWords;
        addWordsToBoard();
    }

    public String getWordsAsString() {
        return this.wordsAsString;
    }

    private void addWordsToBoard() {
        ArrayList<String> seedWords = pickSeedWords();
        System.out.println("Words: " + seedWords);
        for (int i = 0; i < seedWords.size(); i++) {
            wordsAsString += shuffleWord(seedWords.get(i));
        }
    }

    private ArrayList<String> pickSeedWords() {
        ArrayList<String> seedWords = new ArrayList<>();
        String word;

        while (seedWords.size() < NUMBER_OF_SEED_WORDS) {
            word = nineLetterWords.get(new Random().nextInt(nineLetterWords.size()));
            if (!seedWords.contains(word)) {
                seedWords.add(word);
            }
        }
        return seedWords;
    }

    private String shuffleWord(String original) {
        ArrayList<Integer> shuffledOrder = createLetterOrder();
        String shuffled = "";

        for (int i = 0; i < FULL_WORD_LENGTH; i++) {
            shuffled += original.charAt(shuffledOrder.get(i));
        }
        return shuffled;
    }

    private ArrayList<Integer> createLetterOrder() {
        ArrayList<Integer> order = new ArrayList<>(ORIGINAL_ORDER);
        boolean found = false;
        while (!found) {
            Collections.shuffle(order);
            if (isValidOrder(order)) {
                found = true;
            }
        }
        return order;
    }

    private boolean isValidOrder(ArrayList<Integer> shuffled) {
        for (int i = 1; i < FULL_WORD_LENGTH; i++) {
            if (!areNeighbors(shuffled, ORIGINAL_ORDER.get(i - 1), ORIGINAL_ORDER.get(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean areNeighbors(ArrayList<Integer> shuffled, int first, int second) {
        int firstIndex = shuffled.indexOf(first) + 1;
        int secondIndex = shuffled.indexOf(second) + 1;

        return NEIGHBORS.get(firstIndex).contains(secondIndex);
    }

    private HashMap<Integer, ArrayList<Integer>> initNeighbors() {
        HashMap<Integer, ArrayList<Integer>> neighbors = new HashMap<>();
        neighbors.put(1, new ArrayList<>(Arrays.asList(2, 4, 5)));
        neighbors.put(2, new ArrayList<>(Arrays.asList(1, 3, 4, 5, 6)));
        neighbors.put(3, new ArrayList<>(Arrays.asList(2, 5, 6)));
        neighbors.put(4, new ArrayList<>(Arrays.asList(1, 2, 5, 7, 8)));
        neighbors.put(5, new ArrayList<>(Arrays.asList(1, 2, 3, 4, 6, 7, 8, 9)));
        neighbors.put(6, new ArrayList<>(Arrays.asList(2, 3, 5, 8, 9)));
        neighbors.put(7, new ArrayList<>(Arrays.asList(4, 5, 8)));
        neighbors.put(8, new ArrayList<>(Arrays.asList(4, 5, 6, 7, 9)));
        neighbors.put(9, new ArrayList<>(Arrays.asList(5, 6, 8)));

        return neighbors;
    }
}


//        // create new words
//        if(boardWordsAsString.isEmpty()) {
//            addWordsToBoard();
//        }
//
//        // redraw board
//        else {
//            ArrayList<String> seedWords = new ArrayList<>();
//
//            for(int i = 0; i < NUMBER_OF_SEED_WORDS; i++) {
//                int startIndex = i * FULL_WORD_LENGTH;
//                int endIndex = startIndex + FULL_WORD_LENGTH;
//                seedWords.add(boardWordsAsString.substring(startIndex, endIndex));
//            }
//
//            for(int i = 0; i < seedWords.size(); i++) {
//                addToBoard(seedWords.get(i), LARGE_BOARDS.get(i));
//            }
//        }

//        System.out.println(boardColorsAsString);
//        if(!boardColorsAsString.isEmpty()) {
//            unpackBoardColors();
//        }
//    }
