package edu.neu.madcourse.maxcorwin.scroggle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;

import edu.neu.madcourse.maxcorwin.R;

public class LoginActivity extends AppCompatActivity {
    private static final String SCROGGLE_PREFS = "ScrogglePreferenceFile";
    private static final String TWO_PLAYER = "Two Player";
    private static final String TOKEN = "Token";
    private static final String EMAIL_STRING = "Email";

    private EditText emailText;
    private String email;
    private final ArrayList<String> emailAddresses
            = new ArrayList<>(Arrays.asList("maxcorwin@yahoo.com", "otherperson@gmail.com"));

    private boolean twoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        twoPlayer = getIntent().getExtras().getBoolean(TWO_PLAYER);
        System.out.println(twoPlayer);
        readFromSharedPreferences();
        if(!email.isEmpty()) {
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra(TWO_PLAYER, twoPlayer);
            startActivity(intent);
        }

        setContentView(R.layout.hw7_activity_login);
        emailText = (EditText) findViewById(R.id.email_textbox);
    }

    private void readFromSharedPreferences() {
        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        email = scroggleSettings.getString(EMAIL_STRING, "");
    }

    public void loginAction(View view) {
        if(isNetworkAvailable()) {
            String token = FirebaseInstanceId.getInstance().getToken().substring(10).split("\"")[0];
            email = emailText.getText().toString();
            if(emailAddresses.contains(email.toLowerCase())) {
                startUserProfileActivity(email.replaceAll("\\.", ","), token);
            }
            else {
                Toast.makeText(LoginActivity.this, "USER NOT FOUND", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            showInternetUnavailable();
        }
    }

    private void startUserProfileActivity(String email, String token) {
        updateSharedPreferences(email);
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(TOKEN, token);
        intent.putExtra(TWO_PLAYER, twoPlayer);
        startActivity(intent);
    }

    private void updateSharedPreferences(String email) {
        SharedPreferences scroggleSettings = this.getApplicationContext().getSharedPreferences(SCROGGLE_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = scroggleSettings.edit();
        editor.putString(EMAIL_STRING, email.toLowerCase());
        editor.apply();
    }

    public void clearTextboxAction(View v) {
        emailText.setText("");
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void showToast(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void showInternetUnavailable() {
        showToast("Internet Unavailable");
    }
}