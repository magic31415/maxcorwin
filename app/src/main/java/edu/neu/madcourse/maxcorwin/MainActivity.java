package edu.neu.madcourse.maxcorwin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import edu.neu.madcourse.maxcorwin.project.FinalProjectMainActivity;

public class MainActivity extends AppCompatActivity {
    private static final String COMMUNICATION = "Communication";
    private static final String TWO_PLAYER = "Two Player";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hw1_main_activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void aboutAction(View v) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void ticTacToeAction(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.tictactoe.MainActivity.class);
        startActivity(intent);
    }

    public void dictionaryAction(View v) {
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivity(intent);
    }

    public void scroggleAction(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.scroggle.MainActivity.class);
        intent.putExtra(COMMUNICATION, false);
        intent.putExtra(TWO_PLAYER, false);
        startActivity(intent);
    }

    public void communicationAction(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.scroggle.MainActivity.class);
        intent.putExtra(COMMUNICATION, true);
        intent.putExtra(TWO_PLAYER, false);
        startActivity(intent);
    }

    public void twoPlayerScroggleAction(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.scroggle.MainActivity.class);
        intent.putExtra(COMMUNICATION, true);
        intent.putExtra(TWO_PLAYER, true);
        startActivity(intent);
    }

    public void trickiestPartAction(View view) {
        Intent intent = new Intent(this, edu.neu.madcourse.maxcorwin.project.TrickiestPartActivity.class);
        startActivity(intent);
    }

    public void finalProjectAction(View view) {
        Intent intent = new Intent(this, FinalProjectMainActivity.class);
        startActivity(intent);
    }

    public void quitAction(View v) {
        finish();
        System.exit(0);
    }
}
