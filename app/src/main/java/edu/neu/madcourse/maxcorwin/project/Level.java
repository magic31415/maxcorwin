package edu.neu.madcourse.maxcorwin.project;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.BufferOverflowException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

import edu.neu.madcourse.maxcorwin.R;
import edu.neu.madcourse.maxcorwin.scroggle.GameActivity;

/**
 * Created by jzstern on 4/17/17.
 */

public class Level {
    private int levelNum;
    private int numImages;
    private int numPairs;
    private int pairSize;
    private ArrayList<GameImage> images;
    private ArrayList<GameImage> selectedImages = new ArrayList<>();

    private final ArrayList<Integer> twoImages = new ArrayList<>(Arrays.asList(1, 2, 11, 12, 51));
    private final ArrayList<Integer> fourImages = new ArrayList<>(Arrays.asList(3, 4, 13, 14, 21, 22, 52, 61));
    private final ArrayList<Integer> nineImages = new ArrayList<>(Arrays.asList(5, 15, 23, 24, 53));

    private final int ARRAY_SIZE = 288000;
    private final int SHAPES_PER_IMAGE = 100;
    private final int TIME_BETWEEN_SHAPES = 10;
    private final int TIMESTAMP_RANGE = SHAPES_PER_IMAGE * TIME_BETWEEN_SHAPES;
    private Double[] doubleX, doubleY, doubleZ;
    private String[] timestamps;

    private String firstTimestamp;
    private Double[] snapShotX = new Double[SHAPES_PER_IMAGE];
    private Double[] snapShotY = new Double[SHAPES_PER_IMAGE];
    private Double[] snapShotZ = new Double[SHAPES_PER_IMAGE];

    private static final int CIRCLE = 1;
    private static final int TRIANGLE = 2;
    private static final int SQUARE = 3;
    private static final int DIAMOND = 4;

    public Level(int levelNum, String[] timestamps, Double[] doubleX, Double[] doubleY, Double[] doubleZ) {
        this.levelNum = levelNum;
        this.timestamps = timestamps;
        this.doubleX = doubleX;
        this.doubleY = doubleY;
        this.doubleZ = doubleZ;

        this.numImages = setNumImages();
        this.pairSize = setPairSize();
        this.numPairs = setNumPairs();

        setImages();
        setShapeTypes();
        Collections.shuffle(images);
    }

    // TODO add complexity
    private int setNumPairs() {
        if(twoImages.contains(levelNum) || levelNum == 4 || levelNum == 31 || levelNum == 61) {
            return 1;
        }
        else if(fourImages.contains(levelNum)) {
            return 2;
        }
        else if(nineImages.contains(levelNum)) {
            return 4;
        }
        else if(pairSize == 2) {
            return 6;
        }
        else if(pairSize == 3) {
            return 4;
        }
        else {
            return 3;
        }
    }

    private int setPairSize() {
        if((levelNum <= 30) || (levelNum == 41) || (levelNum == 51)) {
            return 2;
        }
        else if(levelNum == 31) {
            return 3;
        }
        else if(levelNum == 61) {
            return 4;
        }
        else if((levelNum >= 32) && (levelNum <= 60)) {
            // 2 or 3
            return pickRandom(2) + 1;
        }
        else {
            // 2, 3, or 4
            return pickRandom(3) + 1;
        }
    }

    private int setNumImages() {
        if(twoImages.contains(levelNum)) {
            return 2;
        }
        else if(levelNum == 31) {
            return 3;
        }
        else if(fourImages.contains(levelNum)) {
            return 4;
        }
        else if(nineImages.contains(levelNum)) {
            return 9;
        }
        else {
            return 12;
        }
    }

    private int numUniqueImages() {
        return numPairs + numImages - (numPairs * pairSize);
    }

    private void setImages() {
        images = new ArrayList<>();
        int timestamp, index;
        final Random RAND = new Random();
        // GZIPInputStream gis;

        System.out.println("Unique Images: " + numUniqueImages());
        // for each image that needs to be drawn
        for(int i = 0; i < numUniqueImages(); i++) {
            timestamp = RAND.nextInt(ARRAY_SIZE - TIMESTAMP_RANGE);
            firstTimestamp = timestamps[timestamp];

            // loop through the individual shapes that make up that image
            for(int j = 0; j < SHAPES_PER_IMAGE; j++) {
                index = timestamp + (j * TIME_BETWEEN_SHAPES);
                snapShotX[j] = doubleX[index];
                snapShotY[j] = doubleY[index];
                snapShotZ[j] = doubleZ[index];
            }
            images.add(new GameImage(firstTimestamp, snapShotX, snapShotY, snapShotZ));
        }

        for(int i = 0; i < numPairs; i++) {
            images.get(i).setUnique(false);
            for(int j = 0; j < pairSize - 1; j++) {
                images.add(new GameImage(images.get(i)));
            }
        }

        // Add all images to selectedImages
        for(GameImage image : images) {
            selectedImages.add(image);
        }
    }

    private void setShapeTypes() {
        for(int i = 0; i < images.size(); i++) {
            images.get(i).setShapeType(pickShape(i));
        }
    }

    private int pickShape(int index) {
        if((levelNum <= 10) || (levelNum == 31) || (levelNum == 51) || (levelNum == 61)) {
            return CIRCLE;
        }
        else if(levelNum == 11) {
            return TRIANGLE;
        }
        else if(levelNum == 12) {
            if(index == 0) {
                return TRIANGLE;
            }
            else {
                return CIRCLE;
            }
        }
        else if(levelNum == 13) {
            if(index % 2 == 0) {
                return TRIANGLE;
            }
            else {
                return CIRCLE;
            }
        }
        else if(levelNum == 14) {
            if(index <= 1) {
                return TRIANGLE;
            }
            else {
                return CIRCLE;
            }
        }
        else if (levelNum >= 15 && levelNum <= 20) {
            return pickRandom(2);
        }
        else if(levelNum == 21) {
            return SQUARE;
        }
        else if(levelNum == 22) {
            if(index % 2 == 0) {
                return SQUARE;
            }
            else {
                return CIRCLE;
            }
        }
        else if(levelNum == 23) {
            if(index == 0 || index == 5) {
                return SQUARE;
            }
            else if(index == 1 || index == 6) {
                return CIRCLE;
            }
            else if(index == 2 || index == 7) {
                return TRIANGLE;
            }
            else {
                return pickRandom(3);
            }
        }
        else if(levelNum == 24) {
            if(index <= 1) {
                return SQUARE;
            }
            else if(index <= 3) {
                return CIRCLE;
            }
            else if(index <= 5) {
                return TRIANGLE;
            }
            else {
                return pickRandom(3);
            }
        }
        else if(levelNum <= 40) {
            return pickRandom(3);
        }
        else if(levelNum == 41) {
            return DIAMOND;
        }
        else {
            return pickRandom(4);
        }
    }

    private static int pickRandom(double choices) {
        double random = Math.random();

        for(double i = 1.0; i < choices; i++) {
            if(random < i/choices) {
                return (int) i;
            }
        }
        return (int) choices;
    }

    public ArrayList<GameImage> getSelectedImages() {
        return selectedImages;
    }

    public void addToSelectedImages(GameImage image) {
        if(!selectedImages.contains(image)) {
            selectedImages.add(image);
        }
    }

    public ArrayList<GameImage> getImages() {
        return images;
    }

    public int getLevelNum() {
        return levelNum;
    }

    public int getNumImages() {
        return numImages;
    }

    public int getPairSize() {
        return pairSize;
    }

    public void clearSelectedImages() {
        selectedImages.clear();
        for(GameImage image : images) {
            image.setSelected(false);
        }
    }

    public void reset() {
        for(GameImage image : images) {
            image.setSelected(true);
            image.setOnBoard(true);
            addToSelectedImages(image);
        }
    }

}
