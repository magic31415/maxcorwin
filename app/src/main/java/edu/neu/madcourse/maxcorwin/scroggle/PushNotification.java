package edu.neu.madcourse.maxcorwin.scroggle;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Max on 3/3/17.
 */

public class PushNotification {

    private static final String SERVER_KEY = "key=AAAAGGKwces:APA91bFc1NEUV_TtbfMPZgHTLKGPqT5IatRdLXG7gb7F6LkaqAdeBp2YqD4WYvXwQCw8lnJgmf7s6ZGPLsOS8rN5GbRO8YrOoPWihWM1bN-GKp-ZyJ3WPfh7HoG377A8MYZyfADO3dws";

    class NotificationTask extends AsyncTask<Void, Void, Void> {
        private String clientRegistrationToken;

        public NotificationTask(String clientRegistrationToken) {
            this.clientRegistrationToken = clientRegistrationToken;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONObject jPayload = new JSONObject();
            JSONObject jNotification = new JSONObject();

            try {
                jNotification.put("title", "Word Game Notification");
                jNotification.put("body", "It's your turn in Scroggle");
                jNotification.put("sound", "default");
                jNotification.put("badge", "1");
                jNotification.put("click_action", "OPEN_ACTIVITY_1"); // TODO right place

                jPayload.put("to", clientRegistrationToken);
                jPayload.put("priority", "high");
                jPayload.put("notification", jNotification);

                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Authorization", SERVER_KEY);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);

                // Send FCM message content.
                OutputStream outputStream = conn.getOutputStream();
                outputStream.write(jPayload.toString().getBytes());
                outputStream.close();

                // Read FCM response.
                InputStream inputStream = conn.getInputStream();
                final String resp = convertStreamToString(inputStream);

                Handler h = new Handler(Looper.getMainLooper());
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void sendNotification(String clientRegistrationToken) {
        new NotificationTask(clientRegistrationToken).execute();

    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
